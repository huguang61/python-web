# 系统配置说明文档

## 文件目录

* python-web

  * backend「后台模块」

    * apiback.php

  * dist「生产系统4.0前端模块」

    * static
      * css
      * js
      * Img
    * index.html

  * setup「环境配置模块」

    * src 「环境配置模块核心依赖文件」
    * setup3.0.sh「旧方式环境配置脚本」
    * setup4.0.sh 「生产系统4.0环境配置脚本」

  * src「核心源码模块」

    * comm「旧方式，采用函数式编程的公共模块」
    * modpcba「旧方式，采用函数式编程的pcba测试的核心模块」
    * modproduct「旧方式，采用函数式编程的product测试的核心模块」
    * modsys4 「生产系统4.0方式的执行层」
    * modthrough「旧方式，采用函数式编程的throughput测试的核心模块」
    * pcba.py「旧方式pcba测试的主入口」
    * product.py旧方式pcba测试的主入口」
    * system4.0.py「生产系统4.0执行层和通信层的主入口」
    * throughput-4.py「旧方式生产4.0过度测试的主入口」
    * throughput-4.1.py「旧方式生产4.0过度测试的主入口」
    * throughput-core.py「旧方式core板测试的主入口」
    * throughput.py「旧方式生产3.0测试的主入口」

  * ssh「桌面启动模块」

    * pcba.sh
    * product.sh
    * throughput-4.1.sh
    * throughput-4.sh
    * throughput-core.sh
    * throughput.sh

  * tools「工具箱模块」

    *

  * unit 「单元测试模块」**注意**：单元测试集还在建立中。会系统的形成单元测试模块

  * View「前端显示模块」

    * static
    * pcba.html
    * product.html
    * throughput.html

## 系统开发说明

**注意**：此份说明主要是针对生产系统4.0的开发和维护进行详尽的说明，旧方式予以放弃不再维护处理。但是仍兼容和维护过渡方式。

### 部署说明

安装系统后，设置显示和键盘选项,打开ssh登录

直接从源码中使用scp获取到根目录下的setup.sh然后修改里面的ip地址为当前源码服务器所在的ip地址，然后运行setup.sh即可

最后设置下用户密码「passwd」为goodlife

### 开发说明

在这里主要是指src这个文件夹。这个是整个系统运行的核心源码文件。而源码根目录文件夹下，又分为各大模块文件夹和入口文件



### 单元测试说明

暂未建立。待定







