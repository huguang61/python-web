#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@author: huyiyong
@file: web.py
@time: 2017/5/11 14:28
"""
import time
import json
import os
from datetime import datetime
from comm.comm import *
import platform
from modsys4.sys4 import SystemF
import comm.config as config
if config.methods == 'old':
    from comm.device import Device

os_type = platform.platform()
bow = start_firefox('pcba', os_type)
device_ip = ""
device_type = ""
tn = ""
def continue_next_test(bow):
    js = "document.getElementById(\"button-default\").setAttribute(\"class\", \"btn btn-lg btn-danger\");"
    bow.execute_script(js)
    js = '$(\"#button-default\").text(\"正在连接中...\")'
    bow.execute_script(js)
    # 2
    js = '$("#button-led-on").hide()'
    bow.execute_script(js)
    js = "document.getElementById(\"button-led-on\").setAttribute(\"class\", \"btn btn-lg btn-default\");"
    bow.execute_script(js)
    js = '$(\"#button-led-on\").text(\"测试灯\")'
    bow.execute_script(js)

    js = "document.getElementById(\"button-reset\").setAttribute(\"class\", \"btn btn-lg btn-default\");"
    bow.execute_script(js)
    js = '$("#button-reset").hide()'
    bow.execute_script(js)
    js = '$(\"#button-reset\").text(\"请按下复位按钮\")'
    bow.execute_script(js)

    js = "document.getElementById(\"button-meshset\").setAttribute(\"class\", \"btn btn-lg btn-default\");"
    bow.execute_script(js)
    js = '$("#button-meshset").hide()'
    bow.execute_script(js)
    js = '$(\"#button-meshset\").text(\"请按下mesh按钮\")'
    bow.execute_script(js)

    js = "document.getElementById(\"button-test\").setAttribute(\"class\", \"btn btn-lg btn-default\");"
    bow.execute_script(js)
    js = '$("#button-test").hide()'
    bow.execute_script(js)
    js = '$("#button-test").text("请拨动开关至少两个来回")'
    bow.execute_script(js)

    js = "document.getElementById(\"button-sd\").setAttribute(\"class\", \"btn btn-lg btn-default\");"
    bow.execute_script(js)
    js = '$("#button-sd").hide()'
    bow.execute_script(js)
    js = '$("#button-sd").text("请插入sd卡设备, 等待几秒")'
    bow.execute_script(js)

    js = "document.getElementById(\"button-usb\").setAttribute(\"class\", \"btn btn-lg btn-default\");"
    bow.execute_script(js)
    js = '$("#button-usb").hide()'
    bow.execute_script(js)
    js = '$("#button-usb").text("请插入USB设备, 等待几秒")'
    bow.execute_script(js)
    return True
def wait_ping_ip_start(ip, timeout=1):
    print("wait_ping_ip_start()")
    res = ""
    cmd = 'ping ' + ip + " -c 1"
    while timeout:
        print "ping ----", ip, res
        try:
            res = os.popen(cmd).read()
        except:
            return False
        if "TTL" in res or 'ttl' in res:
            return True
        timeout -= 1
        time.sleep(0.3)
        print("wait_ping_ip_start() , remain times is {}".format(timeout))
    return False
# 兼容统一体，作为过渡系统
class Action(object):
    pass

while True:
    try:
        print "start"
        device_type = get_test_device_type(bow)
        if device_type is False:
            continue
        print "route type is ", device_type
        device_ip = get_ip(device_type, bow)
        if device_ip is False:
            continue
        if wait_ping_ip_start(device_ip) is False:
            continue
        action = Action()
        action.device_type = device_type
        action.step = 'first'
        if "GL-B1300" == device_type and config.methods == "old":
            config.device = SystemF(action) # 当切换到4.0这套彻底失效
        else:
            config.device = Device(action)
            # config.device.connect_device()
        js = "document.getElementById(\"button-default\").setAttribute(\"class\", \"btn btn-lg btn-success\");"
        bow.execute_script(js)
        js = '''$(\"#button-default\").text(\"已连接路由器!\")'''
        bow.execute_script(js)

        if config.device.calibration() is False:
            js = '''$(\"#button-default\").text(\"设备没有校验，先校验!\")'''
            bow.execute_script(js)
            time.sleep(5)
            continue_next_test(bow)
            continue
        js = '''$(\"#button-default\").text(\"已经校验\")'''
        bow.execute_script(js)

        if config.device.checkMem() is False:
            js = '''$(\"#button-default\").text(\"内存少于标准的百分之七十!\")'''
            bow.execute_script(js)
            time.sleep(5)
            continue_next_test(bow)
            continue
        js = '''$(\"#button-default\").text(\"内存达标\")'''
        bow.execute_script(js)

        time.sleep(1)
        if config.device.checkfirst():
            js = '''$(\"#button-default\").text(\"已通过pcba检测,请不要进行重复检测\")'''
            bow.execute_script(js)
            time.sleep(5)
            continue_next_test(bow)
            continue
        js = '''$(\"#button-default\").text(\"正在测试中\")'''
        bow.execute_script(js)
        js = '$("#button-reset").show()'
        bow.execute_script(js)
        if config.device.reset() is False:
            js = '''$(\"#button-reset\").text(\"重置按钮测试失败!\")'''
            bow.execute_script(js)
            time.sleep(5)
            continue_next_test(bow)
            continue
        js = "document.getElementById(\"button-reset\").setAttribute(\"class\", \"btn btn-lg btn-success\");"
        bow.execute_script(js)
        js = '''$(\"#button-reset\").text(\"重置按钮测试成功\")'''
        bow.execute_script(js)
        if "GL-B1300" == device_type:
            js = '$("#button-meshset").show()'
            bow.execute_script(js)
            if config.device.meshset() is False:
                js = '''$(\"#button-meshset\").text(\"mesh按钮测试失败!\")'''
                bow.execute_script(js)
                time.sleep(5)
                continue_next_test(bow)
                continue
            js = "document.getElementById(\"button-meshset\").setAttribute(\"class\", \"btn btn-lg btn-success\");"
            bow.execute_script(js)
            js = '''$(\"#button-meshset\").text(\"mesh按钮测试成功\")'''
            bow.execute_script(js)
        js = '$("#button-led-on").show()'
        bow.execute_script(js)
        if config.device.testled() is False:
            js = '''$(\"#button-led-on\").text(\"led测试失败!\")'''
            bow.execute_script(js)
            time.sleep(5)
            continue_next_test(bow)
            continue
        js = "document.getElementById(\"button-led-on\").setAttribute(\"class\", \"btn btn-lg btn-success\");"
        bow.execute_script(js)
        js = '''$(\"#button-led-on\").text(\"led测试成功\")'''
        bow.execute_script(js)
        if 'GL-B1300' != device_type:
            js = '$("#button-test").show()'
            bow.execute_script(js)
            if config.device.testswitch() is False:
                js = '''$(\"#button-test\").text(\"拨动开关测试失败!\")'''
                bow.execute_script(js)
                time.sleep(5)
                continue_next_test(bow)
                continue
            js = "document.getElementById(\"button-test\").setAttribute(\"class\", \"btn btn-lg btn-success\");"
            bow.execute_script(js)
            js = '''$(\"#button-test\").text(\"拨动开关测试成功\")'''
            bow.execute_script(js)
        if "GL-AR750" == device_type:
            js = '$("#button-sd").show()'
            bow.execute_script(js)
            if config.device.testsd() is False:
                js = '''$(\"#button-sd\").text(\"sd卡测试失败!\")'''
                bow.execute_script(js)
                time.sleep(5)
                continue_next_test(bow)
                continue
            js = "document.getElementById(\"button-sd\").setAttribute(\"class\", \"btn btn-lg btn-success\");"
            bow.execute_script(js)
            js = '''$(\"#button-sd\").text(\"sd卡测试成功\")'''
            bow.execute_script(js)
        js = '$("#button-usb").show()'
        bow.execute_script(js)
        if config.device.testusb() is False:
            js = '''$(\"#button-usb\").text(\"usb测试失败!\")'''
            bow.execute_script(js)
            time.sleep(5)
            continue_next_test(bow)
            continue
        js = "document.getElementById(\"button-usb\").setAttribute(\"class\", \"btn btn-lg btn-success\");"
        bow.execute_script(js)
        js = '''$(\"#button-usb\").text(\"usb测试成功\")'''
        bow.execute_script(js)
        if config.device.firstok() is False:
            time.sleep(5)
            continue_next_test(bow)
            continue
        js = '''$(\"#button-default\").text(\"完成测试请换下一个设备\")'''
        bow.execute_script(js)
        time.sleep(5)
        continue_next_test(bow)
        time.sleep(0.1)
    except Exception as e:
        time.sleep(10)
        continue_next_test(bow)
        continue
time.sleep(50)
