#coding:utf-8
import os
import requests
import time
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
## 获取code值
def get_code_of_rou(count,bow):
    ro_id = "rou_info" + str(count)
    code = bow.find_element_by_id(ro_id).get_attribute("value")
    if len(code) != 24 and len(code) != 42 and len(code) != 17:
        status_id = "status" + str(count)
        js = '$("#%s").text("长度不对")' % status_id  # code  qj00001 6ae6ef0823d24925
        bow.execute_script(js)
        return False
    return code

def get_two_par_from_code_tuple(code_tuple):
    code_len = code_tuple[-1]
    if code_len == 2 or code_len == 3:
        return code_tuple[0]
    elif code_len == 1:
        return get_ddns_code_from_mac(code_tuple[0])
    return False
def begin_again(bow):
    js = '$("#begin").text("开始测试")'
    bow.execute_script(js)
    js = "document.getElementById(\"begin\").setAttribute(\"class\",\"btn btn-lg btn-danger\");"
    bow.execute_script(js)
    js = '$("#begin").attr("disabled", false)'
    bow.execute_script(js)

def get_ddns_code_from_mac(mac):
    payload = {'action':'mac2ddns','mac_address':mac}
    r = requests.get("http://192.168.16.17/api.php", params=payload)
    return r.json()['ddns_name']

def split_code(code):
    codeList = code.split()
    if len(codeList) == 3:
        if ":" in codeList[0]:  # E4:95:6E:42:14:18 da21418 15777267fa5b8c35
            if len(codeList[1]) > len(codeList[2]):
                return codeList[2], codeList[1], codeList[0], 3
            else:
                return codeList[1], codeList[2], codeList[0], 3
        elif ":" in codeList[2]:  # da21418 15777267fa5b8c35 E4:95:6E:42:14:18
            if len(codeList[0]) > len(codeList[1]):
                return codeList[1], codeList[0], codeList[2], 3
            else:
                return codeList[0], codeList[1], codeList[2], 3
        elif ":" in codeList[1]:
            if len(codeList[0]) > len(codeList[2]):
                return codeList[2], codeList[0], codeList[1], 3
            else:
                return codeList[0], codeList[2], codeList[1], 3
    elif len(codeList) == 2:
        return codeList[0], codeList[1], 2
    elif len(codeList) == 1:
        return codeList[0], 1

def set_ssid_of_name(mac,device_type):
        last_3_mac = "".join(str(mac).split(":"))[-3:]
        ssid_info = "GL-AR150-"
        if "GL-AR300M" in str(device_type).strip():
            ssid_info = "GL-AR300M-"
        elif "GL-AR750" in str(device_type).strip():
            ssid_info = "GL-AR750-"
        elif "GL-USB150" in str(device_type).strip():
            ssid_info = "GL-USB150-"
        elif "GL-B1300" in str(device_type).strip():
            ssid_info = "GL-B1300-"
        elif "GL-MT300N-V2" == str(device_type).strip():
            ssid_info = "GL-MT300N-V2-"
        elif "GL-MIFI" in str(device_type).strip():
            ssid_info = "GL-MIFI-"
        elif "GL-MT300A" == str(device_type).strip():
            ssid_info = "GL-MT300A-"
        elif "GL-MT300N" == str(device_type).strip():
            ssid_info = "GL-MT300N-"
        elif "GL-CORE" == str(device_type).strip():
            ssid_info = "Domino-"
        ssid = ssid_info + last_3_mac.lower()
        if "Lite" in str(device_type).strip():
            ssid += "-NOR"
        print("ssid is {}".format(ssid))
        return ssid
def get_test_device_type(bow):
    pass_route = bow.find_element_by_id("select-route").get_attribute("value").encode('UTF-8')
    if pass_route == "":
      return False
    js = '$(\"#device_type\").text(\"%s\")'%pass_route
    bow.execute_script(js)
    return pass_route
## 从扫码获取到Mac地址
def get_mac_from_code(code):
    print "the code is:",code
    print "the len of code:",len(code)
    if len(code) == 17:
        return code
    else:
        code_tuple = split_code(code)
        ddns_name = get_two_par_from_code_tuple(code_tuple)
        payload = { 'action': 'getDeviceInfo', 'ddns_name': ddns_name }
        re = requests.get("http://192.168.16.17/api.php", params=payload)
        print re.json()[0]
    return re.json()[0]['mac_address']
def start_firefox(html):
    capabilities = webdriver.DesiredCapabilities().FIREFOX
    capabilities["marionette"] = False
    binary = FirefoxBinary(r'/usr/bin/firefox')
    bow = webdriver.Firefox(firefox_binary=binary, capabilities=capabilities)
    bow.get("file:///python-web/view/"+html+".html")
    bow.maximize_window()
    return bow
num = 101
ssidlist = []
maclist = []
readylist = []
bow = start_firefox('tt')
while num:
    try:
        name = bow.find_element_by_id("begin").text
        if name == u"开始测试...":
            js = '$("#begin").text("正在测试中")'
            bow.execute_script(js)
            js = "document.getElementById(\"begin\").setAttribute(\"class\",\"btn btn-lg btn-success\");"
            bow.execute_script(js)
            tryNum = 0;
            for i in range(1,num):
                if tryNum < 2:
                    code = get_code_of_rou(i,bow)
                    if code is False:
                        tryNum+=1
                        maclist.append("life{}".format(i))
                        ssidlist.append('good0{}'.format(i))
                    else:
                        mac = get_mac_from_code(code)
                        maclist.append(mac)
                        device_type = get_test_device_type(bow)
                        ssid = set_ssid_of_name(mac,device_type)
                        ssidlist.append(ssid)
            t1 = time.time()
            print ssidlist
            print maclist
            listLen = len(ssidlist)-tryNum
            while listLen and (time.time()-t1) < 10*60:
                os.popen("sudo rm -rf ./txt.text")
                os.popen("sudo iwlist wlan0 scanning>>./txt.text")
                res = os.popen("cat ./txt.text|grep 'ESSID'|cut -d ':' -f 2").read().replace("\n", ' ').strip().split(" ")
                res1 = os.popen("cat ./txt.text|grep 'Address'|awk '{print $5}'").read().replace("\n", ' ').strip().split(" ")
                os.popen("sudo rm -rf ./txt.text")
                print res
                print res1
                print len(res) == len(res1)
                if len(res) == len(res1) and len(ssidlist):
                    for index, item in enumerate(res):
                        nitem = item.strip('"')
                        if nitem in ssidlist:
                            if nitem not in readylist:
                                print nitem
                                dex = ssidlist.index(nitem)
                                state_id = "status" + str(dex+1)
                                if res1[index] == maclist[dex]:
                                    js = "document.getElementById(\"{}\").setAttribute(\"class\",\"btn btn-lg btn-success\");".format(state_id)
                                    bow.execute_script(js)
                                    js = '$("#{}").text("升级成功")'.format(state_id)
                                    bow.execute_script(js)
                                else:
                                    js = '$("#{}").text("mac地址不匹配，传入:{}真实:{}")'.format(state_id,maclist[dex],res1[index])
                                    bow.execute_script(js)
                                print dex
                                listLen-=1
                                readylist.append(nitem)
                                print "当前运行时间为：{}".format(time.time() - t1)
            maclist = []
            ssidlist = []
            readylist = []
            begin_again(bow)
    except Exception,e:
        print "发生异常{}".format(e)
        maclist = []
        ssidlist = []
        readylist = []
        begin_again(bow)
        continue
