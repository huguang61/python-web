#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@author: huyiyong
@file: powerTest.py
@time: 2017/11/28 14:28
"""
import platform
import time
import re
import sys
import socket
import os
import multiprocessing
import requests
import comm.config as config
from comm.comm import *
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

## 检测是否开始
def check_test3_start(bow,device_type):
    name = bow.find_element_by_id("begin").text
    if name == u"开始测试...":
        config.startflg = False
        js = '$("#begin").text("正在测试中")'
        bow.execute_script(js)
        js = "document.getElementById(\"begin\").setAttribute(\"class\",\"btn btn-lg btn-success\");"
        bow.execute_script(js)
        return True
    else:
        if config.startflg:
            time.sleep(2)
            return False
        else:
            return True
## 启动浏览器
def start_firefox(html,os_type):
    if "Window" in os_type:
        bow = webdriver.Firefox()
        bow.get("file:///d:/python-web/view/"+html+".html")
    else:
        capabilities = webdriver.DesiredCapabilities().FIREFOX
        capabilities["marionette"] = False
        binary = FirefoxBinary(r'/usr/bin/firefox')
        bow = webdriver.Firefox(firefox_binary=binary, capabilities=capabilities)
        bow.get("file:///python-web/view/"+html+".html")
        bow.maximize_window()
    return bow
## 计算下一个mac地址
def computmac(macaddr):
    last_two = "".join(macaddr.split(":"))
    last_two_int = int(last_two,16)
    new_last_two_int = last_two_int + 1
    new_last_two = hex(new_last_two_int)
    new_last_two = new_last_two[2:-1]
    for i in range(len(new_last_two),12):
        new_last_two = '0'+str(new_last_two)
    new_addr = ""
    for item in range(1,13):
        if item % 2 == 0:
            if item == 12:
                new_addr = new_addr + new_last_two[item-2:item]
            else:
                new_addr =new_addr + new_last_two[item-2:item]+":"
    return new_addr.upper()
## 从扫码获取到Mac地址
def get_mac_from_code(code):
    print "the code is:",code
    print "the len of code:",len(code)
    if len(code) == 17:
        return code
    else:
        code_tuple = split_code(code)
        ddns_name = get_two_par_from_code_tuple(code_tuple)
        payload = { 'action': 'getDeviceInfo', 'ddns_name': ddns_name }
        re = requests.get("http://192.168.16.17/api.php", params=payload)
        print re.json()[0]
    return re.json()[0]['mac_address']

def begin_again(bow,device_type="linux"):
    config.startflg = True
    js = '$("#begin").text("开始测试")'
    bow.execute_script(js)
    js = "document.getElementById(\"begin\").setAttribute(\"class\",\"btn btn-lg btn-danger\");"
    bow.execute_script(js)
    js = '$("#begin").attr("disabled", false)'
    bow.execute_script(js)
## 从count获取需要标志的信息
def get_info_from_count(count):
    power_of_port = count_to_power_port[count]
    rou_info_id = "rou_info" + str(count)
    state_id = "status" + str(count)
    return power_of_port,rou_info_id,state_id
## 获取code值
def get_code_of_rou(id,status_id,bow):
    code = bow.find_element_by_id(id).get_attribute("value")
    if len(code) != 24 and len(code) != 42 and len(code) != 17:
        js = '$("#%s").text("长度不对")' % status_id  # code  qj00001 6ae6ef0823d24925
        bow.execute_script(js)
        time.sleep(1)
        return False
    return code
## 将测试三的数据写入数据库中
def write_test3_data_to_database(speed_down_re,speed_on_re,ddns_name):
    print "begin write to database"
    payload = { 'action': 'updateThrough', 'ddns_name': ddns_name,'throughput_rx': speed_down_re,'throughput_tx': speed_on_re }
    r = requests.get("http://192.168.16.17/api.php", params=payload)
    print r.json()
    print "write to database is over"
    print "begin write to file"
## 比较值
def setcomp(speed,count):
    rout = "rx_pass-route"
    speed_success = bow.find_element_by_id(rout).get_attribute("value")
    int_speed_success = float(re.findall("(.*)\s", speed_success)[0])
    txid = 'tx' + str(count)
    if speed < int_speed_success:
        js = 'document.getElementById("%s").setAttribute("class","btn btn-lg btn-danger");' % txid
        bow.execute_script(js)
        js = '$("#{}").text("{}|{}")'.format(txid, speed,config.device_type)
        bow.execute_script(js)
        return False
    else:
        js = 'document.getElementById("%s").setAttribute("class","btn btn-lg btn-success");' % txid
        bow.execute_script(js)
        js = '$("#{}").text("{}|{}")'.format(txid, speed,config.device_type)
        bow.execute_script(js)
        return True
def do_telnet(device_ip,cmd,wait):
    res = os.popen("telnet.sh {} '{}' {}".format(device_ip,cmd,wait)).read()
    return res
def getsocket(data):
    BUF_SIZE = 1024  #设置缓冲区的大小
    server_addr = ('192.168.16.142', 8888)  #IP和端口构成表示地址
    try :
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  #返回新的socket对象
    except socket.error, msg :
        print "Creating Socket Failure. Error Code : " + str(msg[0]) + " Message : " + msg[1]
        sys.exit()
    client.connect(server_addr)  #要连接的服务器地址
    client.sendall(data)  #发送数据到服务器
    data = client.recv(BUF_SIZE)  #从服务器端接收数据
    client.close()
    print data
    return data
def test(num,count):
    iplist = ['1','2','3','4']
    print count
    cmd = "ifconfig eth0|grep eth0"
    res_list = do_telnet('192.168.8.{}'.format(iplist[count]),cmd,0.5).replace('\n', ' ').split(' ')[-5]
    if num == res_list:
        print "mac compare is ok"
        status_id = "status{}".format(count+1)
        js = '$("#{}").text("{}")'.format(status_id,'Mac 地址比较成功')
        bow.execute_script(js)
        js = "document.getElementById('{}').setAttribute('class','btn btn-lg btn-success');".format(status_id)
        bow.execute_script(js)
        print "开始获取数据"
        res = getsocket('5{}'.format(count))
        print res
        print "结束数据"
        setcomp(res,count+1)
    else:
        print "mac compare is false"
        status_id = "status{}".format(count+1)
        js = '$("#{}").text("{}")'.format(status_id,'Mac 地址比较失败')
        bow.execute_script(js)
    return (num, res_list)
os_type = platform.platform()
bow = start_firefox('powertest',os_type)
filename = __file__
num = 4
while True:
    try:
        print "start"
        config.device_type = get_test_device_type(bow)
        if config.device_type is False:
            continue
        if check_test3_start(bow,config.device_type) is False:
            continue
        maclist = []
        time_out = 1
        while time_out <= 4:
            power_of_port,rou_info_id,state_id = get_info_from_count(time_out)
            code = get_code_of_rou(rou_info_id,state_id,bow)
            if code is False:
                break
            else:
                mac = get_mac_from_code(code)
                maclist.append(mac)
                time_out +=1
        print maclist
        start_time = time.time()
        cores = multiprocessing.cpu_count()
        pool = multiprocessing.Pool(processes=cores)
        pool_list = []
        result_list = []
        count = 0
        for xx in maclist:
            print "开始"
            pool_list.append(pool.apply_async(test, (xx,count, )))
            count+=1
        result_list = [xx.get() for xx in pool_list]
        pool.close()
        pool.join()
        print result_list
        print '并行花费时间 %.2f' % (time.time() - start_time)
        begin_again(bow)
    except Exception,e:
        print e
        print "发生异常"
        begin_again(bow)


    
