#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@author: huyiyong
@file: throughput.py
@time: 2017/10/24 14:28
"""
import platform
import time
import re
import os
import comm.config as config
import requests
from comm.device import Device
from comm.head import count_to_power_port
from comm.comm import *
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

# 兼容统一体，作为过渡系统
class Action(object):
    pass
## 启动浏览器
def start_firefox(html,os_type):
    if "Window" in os_type:
        bow = webdriver.Firefox()
        bow.get("file:///d:/python-web/view/"+html+".html")
    else:
        capabilities = webdriver.DesiredCapabilities().FIREFOX
        capabilities["marionette"] = False
        binary = FirefoxBinary(r'/usr/bin/firefox')
        bow = webdriver.Firefox(firefox_binary=binary, capabilities=capabilities)
        bow.get("file:///python-web/view/"+html+".html")
        bow.maximize_window()
    return bow

## 进行下一次
def begin_again(bow,device_type="linux"):
    js = '$("#begin").text("开始测试")'
    bow.execute_script(js)
    js = "document.getElementById(\"begin\").setAttribute(\"class\",\"btn btn-lg btn-danger\");"
    bow.execute_script(js)
    js = '$("#begin").attr("disabled", false)'
    bow.execute_script(js)

## 计算下一个mac地址
def computmac(macaddr):
    last_two = "".join(macaddr.split(":"))
    last_two_int = int(last_two,16)
    new_last_two_int = last_two_int + 1
    new_last_two = hex(new_last_two_int)
    new_last_two = new_last_two[2:-1]
    for i in range(len(new_last_two),12):
        new_last_two = '0'+str(new_last_two)
    new_addr = ""
    for item in range(1,13):
        if item % 2 == 0:
            if item == 12:
                new_addr = new_addr + new_last_two[item-2:item]
            else:
                new_addr =new_addr + new_last_two[item-2:item]+":"
    return new_addr.upper()

## 检测是否开始
def check_test3_start(bow,count,device_type):
    name = bow.find_element_by_id("begin").text
    if name == u"开始测试...":
        config.startflg = False
        control_power(1)
        control_power(2)
        action = Action()
        action.device_type = device_type
        action.step = 'third'
        config.device = Device(action)
        js = '$("#begin").text("正在测试中")'
        bow.execute_script(js)
        js = "document.getElementById(\"begin\").setAttribute(\"class\",\"btn btn-lg btn-success\");"
        bow.execute_script(js)
        return True
    else:
        if config.startflg:
            time.sleep(2)
            return False
        else:
            return True

## 从count获取需要标志的信息
def get_info_from_count(count):
    power_of_port = count_to_power_port[count]
    rou_info_id = "rou_info" + str(count)
    state_id = "status" + str(count)
    return power_of_port,rou_info_id,state_id

## 从扫码获取到Mac地址
def get_mac_from_code(code):
    print "the code is:",code
    print "the len of code:",len(code)
    if len(code) == 17:
        return code
    else:
        code_tuple = split_code(code)
        ddns_name, scan_get_code_service = get_two_par_from_code_tuple(code_tuple)
        upload_info = ddns_name
        res = requests.get("http://192.168.16.17/search.php?upload_info=" + upload_info)
        if res.status_code != 200:
            print "get error", res.status_code, res.text
            return False
        print res.text
        sqlmac = str(res.text).split("=")[1]
    return sqlmac

## 重新开始
def begin_again(bow,device_type="linux"):
    config.startflg = True
    js = '$("#begin").text("开始测试")'
    bow.execute_script(js)
    js = "document.getElementById(\"begin\").setAttribute(\"class\",\"btn btn-lg btn-danger\");"
    bow.execute_script(js)
    js = '$("#begin").attr("disabled", false)'
    bow.execute_script(js)

## 获取设备类型
def get_test_device_type(bow):
    pass_route = bow.find_element_by_id("select-route").get_attribute("value")
    if pass_route == "":
      return False
    js = '$(\"#device_type\").text(\"%s\")'%pass_route
    bow.execute_script(js)
    return pass_route

## 获取code值
def get_code_of_rou(id,status_id,bow):
    code = bow.find_element_by_id(id).get_attribute("value")
    if len(code) != 24 and len(code) != 42 and len(code) != 17:
        js = '$("#%s").text("长度不对")' % status_id  # code  qj00001 6ae6ef0823d24925
        bow.execute_script(js)
        time.sleep(1)
        return False
    return code
## 将测试三的数据写入数据库中
def write_test3_data_to_database(speed_down_re,speed_on_re,ddns_name):
    print "begin write to database"
    url = "http://192.168.16.17/post_speed.php"
    full_speed = str(round(speed_down_re,2)) + " MBits " + str(round(speed_on_re,2))+" MBits"
    datas = {"ddns_name": ddns_name, 'speed': full_speed}
    print datas
    res = requests.post(url, data=datas)
    print "write to database is over"
    print "begin write to file "

## 设置错误信息
def seterr(desc,count,num=28):
    print "next while begin"
    state_id = "status" + str(count)
    if count != num:
        print "hh"
        config.count+=1
        if count != 27:
            config.device.control_power(config.count+1)
            js = '$("#begin").text("正在测试中")'
            bow.execute_script(js)
            js = "document.getElementById('begin').setAttribute('class','btn btn-lg btn-success');"
            bow.execute_script(js)
            js = '$("#{}").text("{}")'.format(state_id,desc)
            bow.execute_script(js)
    else:
        print "while down"
        js = "document.getElementById('begin').setAttribute('class','btn btn-lg btn-success');"
        bow.execute_script(js)
        js = '$("#{}").text("{}")'.format(state_id,desc)
        bow.execute_script(js)
        config.count = 1
        config.device.control_power(1)
        config.device.control_power(2)
        print "next while begin"
        begin_again(bow)

## 比较speed值,并进行下一次的更新
def setcomp(desc,speed,count):
    rout = "{}_pass-route".format(desc)
    speed_success = bow.find_element_by_id(rout).get_attribute("value")
    int_speed_success = int(re.findall("(.*)\s", speed_success)[0])
    speed = round(speed,2)
    txid = desc + str(config.count)
    if speed < int_speed_success:
        js = 'document.getElementById("%s").setAttribute("class","btn btn-lg btn-danger");' % txid
    else:
        js = 'document.getElementById("%s").setAttribute("class","btn btn-lg btn-success");' % txid
    bow.execute_script(js)
    js = '$("#%s").text("%s")' % (txid, speed)
    bow.execute_script(js)

## 控制电源选项
def control_power(count, timeout = 20):
    power_port = count_to_power_port[count]
    cmd = "ctl_client -h 192.168.2.1 -p 1234 -n " + str(power_port)
    while timeout:
        res = os.popen(cmd).read()
        print "ctl_client: ", res
        if "OK" in res:
            return True
        timeout -= 1
        logging.info("control_power(self, count, timeout = 20) , remain times is {}".format(timeout))
    return False

os_type = platform.platform()
bow = start_firefox('throughput',os_type)
filename = __file__
num = 28

while True:
    try:
        print "start"
        count = config.count
        device_type = get_test_device_type(bow)
        if device_type is False:
            continue
        if check_test3_start(bow,count,device_type) is False:
            continue
        power_of_port,rou_info_id,state_id = get_info_from_count(count)
        print "rou_info_id:", rou_info_id
        code = get_code_of_rou(rou_info_id,state_id,bow)
        print code
        if code is False:
            pre_power_of_port,pre_rou_info_id,pre_state_id = get_info_from_count(count-1)
            pre_code = get_code_of_rou(pre_rou_info_id,pre_state_id,bow)
            if pre_code is False:
                seterr("长度不对",count,count)
            else:
                seterr("长度不对",count)
            continue
        mac = get_mac_from_code(code)
        if mac is False:
            seterr("mac地址不存在",count)
            continue
        if config.device.pretreat(mac) is False:
            seterr('连接失败',count)
            continue
        js = "document.getElementById('{}').setAttribute('class','btn btn-lg btn-success');".format(state_id)
        bow.execute_script(js)
        js = '$("#{}").text("{}")'.format(state_id,"正在测试中")
        bow.execute_script(js)
        if config.device.maccompare() is False:
            seterr('mac地址比较失败',count)
            continue
        speed_rx = config.device.speed_rx()
        if speed_rx is False:
            seterr('speed_rx测试失败',count)
            continue
        else:
            setcomp('rx',speed_rx,count)
        speed_tx = config.device.speed_tx()
        if speed_tx is False:
            seterr("speed_tx测试失败",count)
            continue
        else:
            setcomp('tx',speed_tx,count)
        if device_type in "GL-AR750-5G":
            macd = computmac(mac)
            print macd
            code = get_ddns_code_from_mac(macd)
            print code
        else:
            code = get_ddns_code_from_mac(mac)
        print code
        write_test3_data_to_database(speed_rx,speed_tx,code[0])
        seterr('测试成功',count)
    except Exception,e:
        print e
        seterr('发生异常',count)
        continue
