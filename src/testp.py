#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@author: huyiyong
@file: throughput.py
@time: 2017/10/24 14:28
"""
import platform
import time
import re
import os
import comm.config as config
import requests
from modsys4.sys4 import SystemF # 4.1过度系统
from comm.head import count_to_power_port
from comm.comm import *
import time
class Action(object):
    pass
action = Action()
action.device_type = "GL-AR750"
action.step = 'third'
config.device = SystemF(action)
for indexd in range(1,28):
    print("testing power port {}".format(indexd))
    config.device.control_power(indexd)
    time.sleep(5)
