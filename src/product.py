#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@author: huyiyong
@file: web.py
@time: 2017/10/11 14:28
"""
import time
import json
import os
import datetime
from comm.comm import *
import platform
from modsys4.sys4 import SystemF
import comm.config as config
from comm.device import Device

os_type = platform.platform()
bow = start_firefox('product',os_type)
pre_type = ""
# 兼容遗留问题
class Action():
    pass
## 第二轮测试初始化
def continue_second_test(browser):
  js = '$(\"#button-reset\").text(\"请按下复位按钮\")'
  browser.execute_script(js)
  js = '$(\"#button-meshset\").text(\"请按下mesh按钮\")'
  browser.execute_script(js)
  js = '$(\"#button-test\").text(\"请拨动开关至少两个来回\")'
  browser.execute_script(js)

  js = '$("#rou_info").val("")'
  browser.execute_script(js)
  js = '$("#button-second").text("第二次测试")'
  browser.execute_script(js)
  js = "document.getElementById(\"button-second\").setAttribute(\"class\",\"btn btn-lg btn-danger\");"
  browser.execute_script(js)
  js = '$("input").attr("disabled", false)'
  browser.execute_script(js)
  js = '$("#button-reset").hide()'
  browser.execute_script(js)
  js = '$("#button-meshset").hide()'
  browser.execute_script(js)
  js = '$("#button-test").hide()'
  browser.execute_script(js)
  js = '$("#rou_info").focus()'
  browser.execute_script(js)
  pass
def get_second_test_status():
  name = bow.find_element_by_id("button-second").text
  return name
# 检查b1300的code used
def check_new_used(code):
    code_tuple = split_code(code)
    ddns_name = get_two_par_from_code_tuple(code_tuple)
    print ddns_name
    payload = { 'action': 'getDeviceInfo', 'ddns_name': ddns_name }
    r = requests.get("http://192.168.16.17/api.php", params=payload)
    return r.json()[0]
def wait_ping_ip_start(ip, timeout=12):
    print("wait_ping_ip_start()")
    res = ""
    cmd = 'ping ' + ip + " -c 1"
    while timeout:
        print "ping ----", ip, res
        try:
            res = os.popen(cmd).read()
        except:
            return False
        if "TTL" in res or 'ttl' in res:
            return True
        timeout -= 1
        time.sleep(0.3)
        print("wait_ping_ip_start() , remain times is {}".format(timeout))
    return False

# 计算下一个mac地址：
def computemac(macaddr):
  last_two = "".join(macaddr.split(":"))
  last_two_int = int(last_two,16)
  new_last_two_int = last_two_int + 1
  new_last_two = hex(new_last_two_int)
  new_last_two = new_last_two[2:-1]
  for i in range(len(new_last_two),12):
    new_last_two = '0'+str(new_last_two)
  new_addr = ""
  for item in range(1,13):
    if item % 2 == 0:
      if item == 12:
        new_addr = new_addr + new_last_two[item-2:item]
      else:
        new_addr =new_addr + new_last_two[item-2:item]+":"
  return new_addr.upper()
while True:
    try:
        device_type = get_test_device_type(bow)
        if device_type is False:
            continue
        name = get_second_test_status()
        code = get_code_of_rou('rou_info',"button-second",bow)
        if code is False:
            continue
        device_ip = get_ip(device_type,bow)
        if device_ip is False:
            continue
        print "进入"
        if wait_ping_ip_start(device_ip) is False:
            continue
        info = check_new_used(code)
        print info
        file_manufacturer_name = 'PL{}'.format(time.strftime("%Y%m%d%H%M%S",time.localtime(time.time())))
        print file_manufacturer_name
        file_customer_name = 'new_way3'
        test_name = 'pi'
        if info['used'] == '1':
            js = '''$(\"#button-second\").text(\"code被占用，请换下一个设备!\")'''
            bow.execute_script(js)
            time.sleep(5)
            continue_second_test(bow)
            continue
        payload = { 'action': 'updateUsed', 'ddns_name': info['ddns_name'],'used': '1' }
        r = requests.get("http://192.168.16.17/api.php", params=payload)
        print r.json()
        # payload = {'action': 'updateType', 'ddns_name': info['ddns_name'],'product_type':device_type}
        # r = requests.get("http://192.168.16.17/api.php", params=payload)
        # print r.json()
        info1 = ""
        if device_type in ['GL-B1300','GL-AR750']:
            nextmac = computemac(info['mac_address'])
            info1 = check_new_used(nextmac)
            if info1['used'] == '1':
                js = '''$(\"#button-second\").text(\"code被占用，请换下一个设备!\")'''
                bow.execute_script(js)
                time.sleep(5)
                continue_second_test(bow)
                continue
            # payload = { 'action': 'updateUsed', 'ddns_name': info1['ddns_name'],'used': '1' }
            # r = requests.get("http://192.168.16.17/api.php", params=payload)
            # print r.json()
        print "code检测完成"
        action = Action()
        action.device_type = device_type
        action.step = 'second'
        action.mac_address = info['mac_address']
        action.sn_now_use = info['sn_now_use']
        action.sn_back_up = info['sn_back_up']
        action.ddns_name = info['ddns_name']
        if 'GL-B1300' not in device_type:
            action.test_name = file_manufacturer_name
            action.plinfo = file_customer_name
            action.syst = test_name
        if "GL-B1300" == device_type and config.methods == "old":
            config.device = SystemF(action)
        else:
            print "开始进行初始化"
            config.device = Device(action)
            print "初始化完成"

        if config.device.calibration() is False:
            js = '''$(\"#button-second\").text(\"设备没有校验，先校验!\")'''
            bow.execute_script(js)
            time.sleep(5)
            continue_second_test(bow)
            continue
        js = '''$(\"#button-second\").text(\"已经校验\")'''
        bow.execute_script(js)

        if config.device.checkMem() is False:
            js = '''$(\"#button-second\").text(\"内存少于标准的百分之七十!\")'''
            bow.execute_script(js)
            time.sleep(5)
            continue_next_test(bow)
            continue
        js = '''$(\"#button-second\").text(\"内存达标\")'''
        bow.execute_script(js)
        
        time.sleep(1)
        if config.device.checkfirst() is False:
            js = '''$(\"#button-second\").text(\"没通过pcba检测,请进行pcba检测\")'''
            bow.execute_script(js)
            time.sleep(5)
            continue_second_test(bow)
            continue
        js = '''$(\"#button-second\").text(\"已经进行过pcba检测\")'''
        bow.execute_script(js)
        time.sleep(1)
        js = '$("#button-reset").show()'
        bow.execute_script(js)
        if config.device.reset() is False:
            js = '''$(\"#button-reset\").text(\"重置按钮测试失败!\")'''
            bow.execute_script(js)
            time.sleep(5)
            continue_second_test(bow)
            continue
        js = "document.getElementById(\"button-reset\").setAttribute(\"class\", \"btn btn-lg btn-success\");"
        bow.execute_script(js)
        js = '''$(\"#button-reset\").text(\"重置按钮检测成功\")'''
        bow.execute_script(js)
        time.sleep(1)
        if "GL-B1300" == device_type:
            js = '$("#button-meshset").show()'
            bow.execute_script(js)
            if config.device.meshset() is False:
                js = '''$(\"#button-second\").text(\"mesh按钮测试失败!\")'''
                bow.execute_script(js)
                time.sleep(5)
                continue_second_test(bow)
                continue
            js = "document.getElementById(\"button-meshset\").setAttribute(\"class\", \"btn btn-lg btn-success\");"
            bow.execute_script(js)
            js = '''$(\"#button-meshset\").text(\"mesh按钮测试成功\")'''
            bow.execute_script(js)
        if 'GL-B1300' != device_type:
            js = '$("#button-test").show()'
            bow.execute_script(js)
            if config.device.testswitch() is False:
                js = '''$(\"#button-test\").text(\"拨动开关测试失败!\")'''
                bow.execute_script(js)
                time.sleep(5)
                continue_next_test(bow)
                continue
            js = "document.getElementById(\"button-test\").setAttribute(\"class\", \"btn btn-lg btn-success\");"
            bow.execute_script(js)
            js = '''$(\"#button-test\").text(\"拨动开关测试成功\")'''
            bow.execute_script(js)
        if config.device.secondok() is False:
            time.sleep(5)
            continue_second_test(bow)
            continue
        payload = {'action': 'updateType', 'ddns_name': info['ddns_name'],'product_type':device_type}
        r = requests.get("http://192.168.16.17/api.php", params=payload)
        print r.json()
        if device_type in ['GL-B1300','GL-AR750']:
            payload = { 'action': 'updateUsed', 'ddns_name': info1['ddns_name'],'used': '1' }
            r = requests.get("http://192.168.16.17/api.php", params=payload)
            print r.json()
        js = '''$(\"#button-second\").text(\"完成测试请换下一个设备\")'''
        bow.execute_script(js)
        time.sleep(5)
        continue_second_test(bow)
        continue
    except:
        print "发生异常"
        time.sleep(1)
        continue_second_test(bow)
        continue
