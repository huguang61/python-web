#!/usr/bin/env python
# -*- coding: utf-8 -*-
from comm.comm import *
from comm.head import count_to_power_port
import getpass
import requests
## 检测第三步是不是开始
def check_test3_start(bow,count):
  if count == 1:
    name = bow.find_element_by_id("begin").text
    if name != u"开始测试...":
      time.sleep(2)
      return False
    js = '$("#begin").text("正在测试中")'
    bow.execute_script(js)
    js = "document.getElementById(\"begin\").setAttribute(\"class\",\"btn btn-lg btn-success\");"
    bow.execute_script(js)
    return True

##
def get_info_from_count(count):
  power_of_port = count_to_power_port[count]
  rou_info_id = "rou_info" + str(count)
  state_id = "status" + str(count)
  return power_of_port,rou_info_id,state_id

def get_mac_from_code(code):
  print "the code is:",code
  print "the len of code:",len(code)
  if len(code) == 17:
    return code
  else:
    code_tuple = split_code(code)
    ddns_name, scan_get_code_service = get_two_par_from_code_tuple(code_tuple)
    upload_info = ddns_name
    res = requests.get("http://192.168.16.17/search.php?upload_info=" + upload_info)
    if res.status_code != 200:
      print "get error", res.status_code, res.text
      return False
    print res.text
    sqlmac = str(res.text).split("=")[1]
    return sqlmac

def set_ssid_of_name(device_type, last_3_mac):
  ssid_info = "GL-AR150-"
  if "GL-AR300M" == str(device_type).strip():
    ssid_info = "GL-AR300M-"
  elif "GL-MT300N-V2" == str(device_type).strip():
    ssid_info = "GL-MT300N-V2-"
  elif "GL-MIFI" == str(device_type).strip():
    ssid_info = "GL-MIFI-"
  elif "GL-MIFI-nobattery" == str(device_type).strip():
    ssid_info = "GL-MIFI-"
  elif "GL-AR300M-Lite" == str(device_type).strip():
    ssid_info = "GL-AR300M-"
  elif "GL-AR750" == str(device_type).strip():
    ssid_info = "GL-AR750-"
  elif "GL-MT300A" == str(device_type).strip():
    ssid_info = "GL-MT300A-"
  elif "GL-MT300N" == str(device_type).strip():
    ssid_info = "GL-MT300N-"
  elif "GL-CORE" == str(device_type).strip():
    ssid_info = "Domino-"
  ssid = ssid_info + last_3_mac.lower()
  if "GL-AR300M-Lite" in device_type:
    ssid += "-NOR"
  print "ssid is", ssid
  return ssid
## 第三次测试初始化
def begin_again(bow,device_type="linux"):
  js = '$("#begin").text("开始测试")'
  bow.execute_script(js)
  js = "document.getElementById(\"begin\").setAttribute(\"class\",\"btn btn-lg btn-danger\");"
  bow.execute_script(js)
  js = '$("#begin").attr("disabled", false)'
  bow.execute_script(js)
  if "GL-CORE" != device_type and "GL-MIFI" != device_type:
      control_power(1)

## 端口通过count控制
def power_on_count_to_port(master_ssh, count, power_of_port):
  time_out = 20
  while True:
    time_out -= 1
    port = power_of_port
    cmd = "ctl_client -h 192.168.2.1 -p 1234 -n " + str(port)
    res = master_ssh.send(cmd)
    print "ctl_client: ", res
    if "OK" in res:
      return True
    if time_out == 0:
      print "power on port is %s fail" %power_of_port, res
      return False
    time.sleep(0.5)
## 通过当前count控制下一个端口
def power_on_count_next(master_ssh, count, power_of_port):
  time_out = 20
  while True:
    if count < 28:
      time_out -= 1
      new_count = count+1
      port = count_to_power_port[new_count]
      cmd = "ctl_client -h 192.168.2.1 -p 1234 -n " + str(port)
      res = master_ssh.send(cmd)
      if "OK" in res:
        print "-------power_of_port next is open ----------"
        print "the count is %s" % new_count
        print "the next power port is %s" % port
        print "--------------------------------------------"
        return True
      if time_out == 0:
        print "power on port is %s fail" % count, res
        return False
      time.sleep(0.5)
    elif count == 28:
      print "count is 28, not next"
## 通过count控制能源端口
def control_power(count):
  ssh = connect_device_by_ssh("192.168.9.1","goodlife1")
  if ssh is False:
    return False
  port = count_to_power_port[count]
  if power_control(ssh,count,port) is False:
      return False
  print "power set is on, wait ping device is connect..."
  ssh.close()
  return True
## 端口控制
def power_control(master_ssh, count, power_of_port):
  if count == 1:
    if power_on_count_to_port(master_ssh, count, power_of_port) is False:
      return False
    if power_on_count_next(master_ssh, count, power_of_port) is False:
      return False
  else:
    if power_on_count_next(master_ssh, count, power_of_port) is False:
      return False
## 设置ssid
def set_ssid(ssid, device_type, os_type="linux"):
  ssh = connect_device_by_ssh("192.168.9.1","goodlife1")
  if ssh is False:
    return False
  print "setting ssid"
  ssh.send_only("/root/setssid.sh "+ssid)
  time.sleep(3)
  ip = "192.168.8.1"
  if device_type == "GL-CORE":
    ip = "192.168.1.1"
  return wait_ping_ip_by_ssh(ssh, ip, ssid, os_type)
## 设置ssid有端口控制板
def set_ssid_mul(ssid, count, power_of_port,test_device_type, os_type="linux"):
  ssh = connect_device_by_ssh("192.168.9.1","goodlife1")
  if ssh is False:
    return False
  print "setting ssid"
  ssh.send_only("/root/setssid.sh "+ssid)
  print "power %s is being on" % count
  if power_control(ssh, count, power_of_port) is False:
    return False
  print "power set is on, wait ping device is connect..."
  time.sleep(3)
  ip = "192.168.8.1"
  if test_device_type == 'GL-CORE':
    ip = "192.168.1.1"
  return wait_ping_ip_by_ssh(ssh, ip, ssid, os_type)
## 设置测试三的运行模式
def run_mode(filename, device_type,ssid,count,power_of_port,os_type):
  if "3-11" in filename:
    if set_ssid(ssid,device_type):
      return True
    else:
      return False
  elif "GL-MIFI" == str(device_type).strip():
    if set_ssid(ssid,device_type):
      return True
    else:
      return False
  elif "GL-CORE" == device_type:
    if set_ssid(ssid,device_type):
      return True
    else:
      return False
  else:
    if set_ssid_mul(ssid, count, power_of_port,device_type, os_type):
      return True
    else:
      return False
## 测试前预处理
def pre_speed(code, bow, count, power_of_port, device_type, os_type="Window", filename=""):
  status_id = "status" + str(count)
  js = '$("#%s").text("连接设备中...")' % status_id  # code  qj00001 6ae6ef0823d24925
  bow.execute_script(js)
  print "-----------testing-------------"
  print "count is ", count
  print "power of port is ", power_of_port
  print  "-----------------------------"
  test_name = getpass.getuser()
  sqlmac = get_mac_from_code(code)
  print "the sqlmac:",sqlmac
  last_3_mac = "".join(str(sqlmac).split(":"))[-3:]
  print last_3_mac
  if last_3_mac is False:
    return False
  ssid = set_ssid_of_name(device_type, last_3_mac)
  if run_mode(filename, device_type, ssid,count,power_of_port,os_type) is False:
    return False
  return sqlmac
## 开始测试
def test_begin(bow,status_id):
  js = '$("#%s").text("开始测试")'% status_id
  bow.execute_script(js)
  js = 'document.getElementById("%s").setAttribute("class","btn btn-lg btn-success");' % status_id
  bow.execute_script(js)
  print "开始执行命令"
## 从主设备中下载iperf
def wait_wget_success(device_ssh, device_lan_master_ip,time_out, iperf_type="linux"):
  while True:
    print " begin wait wget "
    time_out -= 1
    if iperf_type == "linux":
      res = device_ssh.send("wget http://%s/iperf_linux -O /tmp/iperf_linux -T 2" % device_lan_master_ip, timeout=2)
      print res
    if iperf_type == "mt7620":
      res = device_ssh.send("wget http://%s/iperf_mt7620 -O /tmp/iperf_mt7620 -T 2" % device_lan_master_ip, timeout=2)
      print res
    if iperf_type == "mt7628":
      res = device_ssh.send("wget http://%s/iperf_mt7628 -O /tmp/iperf_mt7628 -T 2" % device_lan_master_ip, timeout=2)
      print res
    if "ETA" in res:
      print "wget is succesed"
      return True
    if time_out == 0:
      print "wget is fail"
      return False
    time.sleep(1)
## 测试下载速度
def test_device_rx(device_ssh,master_ssh,count,bow,device_iperf="linux",master_iperf="linux",device_ip="192.168.8.1", master_ip = "192.168.9.1", device_lan_master_ip = "192.168.8.154"):
  device_ssh.send_only("echo 'connected' > /tmp/process")
  device_ssh.send_only('echo "" >/tmp/speed')
  kill_device_iperf = 'killall -9 iperf_%s'%device_iperf
  device_ssh.send_only(kill_device_iperf)
  if wait_wget_success(device_ssh, device_lan_master_ip, 25, device_iperf) is False:
    return False
  print "-" * 20
  device_chmod_for_iperf = "chmod +x /tmp/iperf_%s"%device_iperf
  device_ssh.send_only(device_chmod_for_iperf)
  device_service_iperf = "/tmp/iperf_%s -s &"%device_iperf
  res = device_ssh.send_expect(device_service_iperf, "TCP window size:")  # 测试被测设备的RX speed_down
  if res is False:
    return False
  print "-"*20
  master_ssh.send_only('echo "TX testing" > /tmp/progress')
  print "speed_down"
  print "-"*20
  master_client_iperf = "/www/iperf_%s -c %s"%(master_iperf,device_ip)
  res = master_ssh.send_expect(master_client_iperf, "Mbits/sec", timeout=25)  # 25 is 12.5s
  if res is False:
    return False
  print "res is ------------------------------------------\n", res
  speed_down_re = re.findall("MBytes\s(.* Mbits)", res)[0].strip()
  print "speed_down_re is ", speed_down_re
  print "over"
  print "-"*20, "获取 speed_down RX"
  rx_id = "rx" + str(count)
  speed_down_float = float(re.findall("(.*)M", speed_down_re)[0])
  speed_success = bow.find_element_by_id("rx_pass-route").get_attribute("value")
  print "speed_success is ", speed_success
  int_speed_success = int(re.findall("(.*)\s", speed_success)[0])
  print "int_speed_success is ", int_speed_success
  if speed_down_float < int_speed_success:
    js = 'document.getElementById("%s").setAttribute("class","btn btn-lg btn-danger");'% rx_id
  else:
    js = 'document.getElementById("%s").setAttribute("class","btn btn-lg btn-success");'%rx_id
  bow.execute_script(js)
  js = '$("#%s").text("%s")'% (rx_id, speed_down_re)
  bow.execute_script(js)
  print "*"*200
  # 被测设备杀死该进程
  kill_device_iperf = 'killall -9 iperf_%s'%device_iperf
  device_ssh.send_only(kill_device_iperf)
  time.sleep(0.2)
  clear_buff = device_ssh.recv()
  print "clear buff begin ==============================="
  print clear_buff
  print "clear buff over ================================"
  return speed_down_re
## 测试上传速度
def test_device_tx(device_ssh, master_ssh,count,bow,device_iperf="linux",master_iperf="linux",device_ip="192.168.8.1", master_ip = "192.168.9.1", device_lan_master_ip = "192.168.8.154"):
  # 在主测设备开启服务
    master_service_iperf = "/www/iperf_%s -s &"%master_iperf
    master_ssh.send_only(master_service_iperf)
    time.sleep(1)
    device_client_iperf = "/tmp/iperf_%s -c %s"%(device_iperf, device_lan_master_ip)
    tx = device_ssh.send_expect(device_client_iperf, "Mbits/sec", timeout=25)
    if tx is False:
        return False
    print "res is ------------------------------------------\n", tx
    speed_on_re = re.findall("MBytes\s(.* Mbits)", tx)[0].strip()
    print "speed_on_re is ", speed_on_re
    tx_id = "tx" + str(count)
    speed_on_float = float(re.findall("(.*)M", speed_on_re)[0])
    speed_success = bow.find_element_by_id("tx_pass-route").get_attribute("value")
    print "speed_success is ", speed_success
    int_speed_success = int(re.findall("(.*)\s", speed_success)[0])
    if speed_on_float < int_speed_success:
        js = 'document.getElementById("%s").setAttribute("class","btn btn-lg btn-danger");' % tx_id
    else:
        js = 'document.getElementById("%s").setAttribute("class","btn btn-lg btn-success");' % tx_id
    bow.execute_script(js)
    kill_master_iperf = 'killall -9 iperf_%s'%master_iperf
    master_ssh.send_only(kill_master_iperf)
    js = '$("#%s").text("%s")' % (tx_id, speed_on_re)
    bow.execute_script(js)
    return speed_on_re

## 将测试三的数据写入数据库中
def write_test3_data_to_database(speed_down_re,speed_on_re,code):
  print "begin write to database"
  url = "http://192.168.16.17/post_speed.php"
  full_speed = speed_down_re + " " + speed_on_re
  code_tuple = split_code(code)
  ddns_name, scan_get_code_service = get_two_par_from_code_tuple(code_tuple)
  datas = {"ddns_name": ddns_name, 'speed': full_speed}
  print datas
  res = requests.post(url, data=datas)
  print "write to database is over"
  print "begin write to file "
## 将测试三的数据写入设备中
def write_test3_data_to_device(device_ssh,master_ssh,speed_down_re, speed_on_re):
  master_ssh.send_only("echo %s %s > /tmp/speed"% (speed_down_re, speed_on_re))
  device_ssh.send_only("echo %s %s > /root/speed"% (speed_down_re, speed_on_re))
  device_ssh.send_only("exit")
  master_ssh.send_only('echo "Finished" >/tmp/progress')
  print "file write over"
  master_ssh.close()
  device_ssh.close()
## mac地址比对
def cmp_mac(ssh,sqlmac,status_id,bow):
  print 'sqlmac is: ', sqlmac
  mac = ssh.send_expect("ifconfig eth0|grep eth0|awk '{print $5}'", "root")
  if mac is False:
      return False
  macd = str(mac[42:59])
  print "the mac is:", macd
  if cmp(macd, sqlmac):
    js = '$("#%s").text("设备mac地址异常")'% status_id
    bow.execute_script(js)
    js = 'document.getElementById("%s").setAttribute("class","btn btn-lg btn-danger");'% status_id
    bow.execute_script(js)
    return False
  return True
def speed(bow, count, power_of_port, code,device_type,sqlmac,device_iperf="linux",master_iperf="linux",device_ip="192.168.8.1", master_ip = "192.168.9.1", device_lan_master_ip = "192.168.8.154"):
    status_id = "status" + str(count)
    print "set status_id"
    try:
      device_ssh = SSHClient(host=master_ip, port=22, username="root", password="goodlife1")
      device_ssh.connect()
      master_ssh = SSHClient(host=master_ip, port=22, username="root", password="goodlife1")
      master_ssh.connect()
    except Exception, e:
      return False,False
    print "get ssh connect"
    test_begin(bow, status_id)
    master_ssh.send_only("rm .ssh/known_hosts")
    ## 通过telnet连接到设备终端,"GL-MIFI-nobattery"
    if device_type == "GL-MT300N-V2":
      res = device_ssh.send("ssh root@192.168.8.1", timeout=4)
      print "connect mt300n-v2:",res
      if "connecting" in res:
        res = device_ssh.send_expect("y", "GL-MT300N-V2", timeout=5)
        print res
        if res is False:
            return False
    elif device_type == "GL-AR750":
      res = device_ssh.send("ssh root@192.168.8.1", timeout=4)
      print "connect GL-AR750:",res
      if "connecting" in res:
        res = device_ssh.send_expect("yes", "GL-AR750", timeout=5)
        print res
        if res is False:
          return False
    else:
      res = device_ssh.send_expect("telnet "+device_ip, "root")
    if res is False:
      return False
    if cmp_mac(device_ssh,sqlmac,status_id,bow) is False:
      js = '$("#%s").text("设备mac地址异常")'% status_id
      bow.execute_script(js)
      write_test3_data_to_database('core-mac-compare','false',code)
      return False
    if device_type == "GL-CORE":
      js = '$("#%s").text("设备mac地址正常")'% status_id
      bow.execute_script(js)
      write_test3_data_to_database('core-mac-compare','true',code)
      return True
    speed_down_re = test_device_rx(device_ssh,master_ssh,count,bow,device_iperf,master_iperf)
    if  speed_down_re is False:
      return False
    speed_on_re = test_device_tx(device_ssh,master_ssh,count,bow,device_iperf,master_iperf)
    if speed_on_re is False:
      return False
    write_test3_data_to_database(speed_down_re,speed_on_re,code)
    # 写入到文件中
    write_test3_data_to_device(device_ssh,master_ssh,speed_down_re,speed_on_re)
    return True
