#!/usr/bin/env python
#encoding: utf-8
import os
import telnetlib
import socket
import fcntl, struct
import paramiko
import subprocess
import logging
import time
import config as config
from datetime import datetime
import re

# paramiko.util.log_to_file("paramiko.log")
# logging.basicConfig(level=logging.DEBUG,
#     format = '[%(asctime)s] %(levelname)s %(message)s',
#     datefmt = '%Y-%m-%d %H:%M:%S')
# root = logging.getLogger()
# root.setLevel(logging.NOTSET)
count_to_power_port = {
    1: 1,
    2: 15,
    3: 2,
    4: 16,
    5: 3,
    6: 17,
    7: 4,
    8: 18,
    9: 5,
    10: 19,
    11: 6,
    12: 20,
    13: 7,
    14: 21,
    15: 8,
    16: 22,
    17: 9,
    18: 23,
    19: 10,
    20: 24,
    21: 11,
    22: 25,
    23: 12,
    24: 26,
    25: 13,
    26: 27,
    27: 14,
    28: 28,
}

class SSHClient():
    def __init__(self, host, port=22, username="root", password='talkingdt'):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.recv_buff = 1024*1024*1024

    def connect(self):
        self.ssh.connect(hostname=self.host, port=self.port, username=self.username, password=self.password)
        self.chan = self.ssh.invoke_shell()
        time.sleep(0.1)
        self.banner = self.chan.recv(self.recv_buff)

    def get_current_ssh_config(self):
        config = {}
        config["host"] = self.host
        config["port"] = self.port
        config["username"] = self.username
        config["password"] = self.password
        return config

    def get_banner(self):
        return self.banner

    def recv_timeout(self, size=1024*1024*1024, timeout=2):
        self.chan.setblocking(0)
        total_data=[];data='';begin=time.time()
        while 1:
            #if you got some data, then break after wait sec
            if total_data and time.time()-begin>timeout:
                break
            #if you got no data at all, wait a little longer
            elif time.time()-begin>timeout*2:
                break
            try:
                data=self.chan.recv(size)
                if data:
                    total_data.append(data)
                    begin=time.time()
                else:
                    time.sleep(0.1)
            except:
                pass
        return ''.join(total_data)

    def recv(self, size=1024*1024*1024):
        return self.chan.recv(size)

    def recv_expect(self, expect, timeout=5):
        buff = ""
        while expect not in buff:
            resp = ""
            try:
                resp = self.chan.recv(self.recv_buff)
            except socket.timeout, e:
                return False
            print "resp is ", str([resp])
            buff += resp
            time.sleep(0.3)
            timeout -= 1
            if timeout == 0:
                return False
        return buff

    def send(self, cmd, timeout=1):
        self.chan.send(cmd + "\n")
        time.sleep(timeout)
        return self.chan.recv(self.recv_buff)

    def send_only(self, cmd, timeout=1):
        self.chan.send(cmd + "\n")
        time.sleep(timeout)

    def close(self):
        self.ssh.close()

    def send_expect(self, cmd, expect, timeout=5):
        self.chan.send(cmd + "\n")
        buff = ""
        while expect not in buff:
            resp = ""
            try:
                resp = self.recv_timeout(self.recv_buff)
            except socket.timeout, e:
                return False
            buff += resp
            time.sleep(0.5)
            timeout -= 1
            if timeout == 0:
                return False
        return buff

class Device(object):
    """
    设备类封装
    """
    def __init__(self, action,mode="2G"):
        self.device_type = action.device_type
        self.step = action.step
        self.mode = mode
        self.setMem()
        if self.step == 'third':
            if "GL-CORE" == self.device_type:
                self.device_ip = '192.168.1.1'
            else:
                self.device_ip = '192.168.8.1'
            self.iperf,self.device_iperf = self.set_iperf()
            try:
                self.tn = self.connect_device(host='127.0.0.1')
                if self.tn is False:
                    return
                self.device_tn = self.connect_device(host='127.0.0.1')
                if self.device_tn is False:
                    return
                self.connect_device()
            except:
                return
        else:
            if self.step == "second":
                self.mac_address = action.mac_address
                self.sn_now_use = action.sn_now_use
                self.sn_back_up = action.sn_back_up
                self.ddns_name = action.ddns_name
                self.test_name = action.test_name
                self.plinfo = action.plinfo
                self.syst = action.syst
            self.device_ip = self.get_ip()
            self.tn = self.connect_device(host="127.0.0.1")
        print("已经初始化好")
        return

    def get_inet_ip(self, ifname):
        cmd = "ifconfig {}".format(ifname)+"|grep 192.168|awk '{print $2}'"
        ip = os.popen(cmd).read().replace("\n"," ").split(" ")[0]
        return ip

    def setMem(self):
        if self.device_type in ['GL-AR150','GL-CORE','GL-USB150','GL-MIFI','GL-MT300A']:
            self.mem = 64*1024*0.7
        elif self.device_type in ['GL-AR300M','GL-AR750','GL-AR750-5G','GL-MT300N','GL-MT300N-V2']:
            self.mem = 128*1024*0.7
        elif sel.device_type in ['GL-B1300']:
            self.mem = 256*1024*0.7
    def command(self,cmd, timeout=60):  
        """执行命令cmd，返回命令输出的内容。 
        如果超时将会抛出TimeoutError异常。 
        cmd - 要执行的命令 
        timeout - 最长等待时间，单位：秒 
        """  
        p = subprocess.Popen(cmd, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)  
        t_beginning = time.time()  
        seconds_passed = 0  
        while True:  
            if p.poll() is not None:  
                break
            seconds_passed = time.time() - t_beginning  
            if timeout and seconds_passed > timeout:  
                p.terminate()
                return False  
            time.sleep(0.1)
        return p.stdout.read()
    # 使用telnet方式登录设备，并执行命令获取反馈
    def do_telnet(self,cmd,wait):
        res = self.command("telnet.sh {} '{}' {}".format(self.device_ip,cmd,wait),30)
        return res
        # 检测内存大小
    def checkMem(self,time_out=2):
        print "checkMem()"
        res = self.do_telnet('free -h|grep Mem', 0.1)
        mem = re.findall("\d+",''.join(res.replace('\n',' ').replace('\r',' ').split(':')[-2:]))[0]
        print "内存大小为：{}".format(mem)
        if int(mem) > self.mem:
            return True
        else:
            return False
    def get_ip(self):
        print("get_ip()")
        if "GL-AR150" == self.device_type or "GL-AR300M" == self.device_type or "GL-AR750" == self.device_type:
            device_ip = "192.168.1.1"
            return device_ip
        elif "GL-MT300N-V2" == self.device_type:
            device_ip = "10.10.10.254"
            return device_ip
        else:
            return False

    def get_ip2(self, ifname):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24])

    def offline(self):
        if self.step == "third":
            try:
                self.tn.close()
                self.device_tn.close()
                return True
            except Exception as e:
                return False
        elif self.step == "first" or self.step == "second":
            try:
                self.tn.close()
                return True
            except Exception as e:
                return False

    def control_power(self, count, timeout = 20):
        power_port = count_to_power_port[count]
        cmd = "ctl_client -h 192.168.2.1 -p 1234 -n " + str(power_port)
        while timeout:
            res = self.tn.send(cmd)
            print "ctl_client: ", res
            if "OK" in res:
                return True
            timeout -= 1
            print("control_power(self, count, timeout = 20) , remain times is {}".format(timeout))
        return False

    def set_iperf(self):
        if "GL-MT300N-V2" == self.device_type:
            master_iperf = "mt7628"
            device_iperf = "mt7628"
        elif self.device_type in ["GL-MT300N",'GL-MT300A']:
            device_iperf = 'mt7620'
            master_iperf = 'linux'
        else:
            device_iperf = 'linux'
            master_iperf = 'linux'
        return master_iperf,device_iperf

    def wait_ping_ip_by_ssh(self, time_out=30):
        cmd = 'ping ' + self.device_ip + " -c 2 -W 2"
        after = datetime.now()
        while time_out:
            try:
                res = self.device_tn.send(cmd, timeout=3)
                print("ping res is :{}".format(res))
            except Exception,e:
                print e
                self.device_tn = self.connect_device(host='127.0.0.1')
                if self.device_tn is False:
                    return False
                continue
            if "ttl" in res or "TTL" in res:
                print("ping通设备成功")
                return True
            time_out-=1
            time.sleep(0.3)
            print("wait_ping_ip_by_ssh() , remain times is {}".format(time_out))
        return False

    def wait_ping_ip_start(self, timeout=12):
        print("wait_ping_ip_start()")
        res = ""
        cmd = 'ping ' + self.device_ip + " -c 1"
        while timeout:
            print "ping ----", self.device_ip, res
            try:
                res = os.popen(cmd).read()
            except:
                return False
            if "TTL" in res or 'ttl' in res:
                return True
            timeout -= 1
            time.sleep(0.3)
            print("wait_ping_ip_start() , remain times is {}".format(timeout))
        return False

    def connect_telnet_host(self,host="192.168.1.1", timeout=5):
        try:
            tn = telnetlib.Telnet()
            tn.open(host, timeout=2)
            banner = tn.read_until("/#", timeout=2)
            print("the banner is: {}".format(banner))
        except Exception:
            return False
        return tn

    def connect_device(self,host="127.0.0.1",password="goodlife"):
        if self.step == 'third':
            try:
                ssh = SSHClient(host=host,port=22,username="pi",password=password)
                ssh.connect()
            except Exception, e:
                ssh.close()
                print(e)
                return False
            return ssh
        else:
            try:
                res = self.wait_ping_ip_start()
            except:
                return False
            if res is False:
                return False
            print("已连接")
            try:
                tn = self.connect_telnet_host(host=self.device_ip)
            except Exception, e:
                print(e)
                return False
            if tn is False:
                return False
            print("已连接正在测试中")
            return tn

    def waitdevice(self):
        print("登录ssh")
        try:
            self.tn.send_only("rm -f /home/pi/.ssh/known_hosts")
        except Exception,e:
            print e
            pass
        if self.device_type == 'GL-MT300N-V2' or "GL-AR750" in self.device_type:
            print("通过ssh登录设备")
            try:
                cmd = "ssh root@192.168.8.1"
                res = self.device_tn.send_expect(cmd,"connecting",timeout=2)
            except Exception, e:
                print(e)
                return False
            if res:
                print("res 中有connecting")
                try:
                    res = self.device_tn.send_expect("yes",'root', timeout=2)
                    if res:
                        print("登录ssh成功")
                        return True
                    else:
                        res = self.device_tn.send_expect("y",'root', timeout=2)
                        if res is False:
                            return False
                        else:
                            return True
                except Exception, e:
                    print(e)
                    return False
            else:
                print("res 中没有connecting")
                try:
                    cmd = "ssh root@192.168.8.1"
                    res = self.device_tn.send_expect(cmd,"raspberry",timeout=3)
                    if res is False:
                        print("登录ssh成功")
                        return True
                    else:
                        print "登录ssh失败"
                        return False
                except Exception,e:
                    print e
                    return False
        else:
            try:
                print("通过telnet登录设备")
                cmd = "telnet {}".format(self.device_ip)
                print "telnet 命令是{}".format(cmd)
                res = self.device_tn.send_expect(cmd,'root',timeout=5)
            except Exception, e:
                print(e)
                return False
            if res is False:
                return False
            return True

    def set_ssid_of_name(self):
        last_3_mac = "".join(str(self.mac).split(":"))[-3:]
        ssid_info = "GL-AR150-"
        if "GL-AR300M" == str(self.device_type).strip():
            ssid_info = "GL-AR300M-"
        elif "GL-AR750" == str(self.device_type).strip():
            ssid_info = "GL-AR750-"
        elif "GL-AR750-5G" == str(self.device_type).strip():
            ssid_info = "GL-AR750-"
            self.mode = "5G"
        elif "GL-MT300N-V2" == str(self.device_type).strip():
            ssid_info = "GL-MT300N-V2-"
        elif "GL-MIFI" in str(self.device_type).strip():
            ssid_info = "GL-MIFI-"
        elif "GL-AR300M-Lite" == str(self.device_type).strip():
            ssid_info = "GL-AR300M-"
        elif "GL-MT300A" == str(self.device_type).strip():
            ssid_info = "GL-MT300A-"
        elif "GL-MT300N" == str(self.device_type).strip():
            ssid_info = "GL-MT300N-"
        elif "GL-CORE" == str(self.device_type).strip():
            ssid_info = "Domino-"
        ssid = ssid_info + last_3_mac.lower()
        if "GL-AR300M-Lite" == str(self.device_type).strip():
            ssid += "-NOR"
        if self.mode == "5G":
            ssid = ssid+"-5G"
        print("ssid is {}".format(ssid))
        return ssid

    def set_ssid(self):
        print('setssid')
        ssid = self.set_ssid_of_name()
        try:
            connect = os.popen("ssid.sh {}".format(ssid))
            result = connect.readlines()
            print("connect result is: %s" % result[-1])
            if "connected" in result[-1]:
                print('连接成功')
            else:
                return False
        except Exception,e:
            print(e)
            return False
        ip = '192.168.8.1'
        if "Domino" in ssid:
            ip = '192.168.1.1'
        if self.wait_ping_ip_by_ssh() is False:
            print('ssh ping失败')
            return False
        if self.waitdevice() is False:
            print("连接ssh失败")
            return False
        return True

    def pretreat(self,mac):
        self.mac = mac
        if self.set_ssid() is False:
            return False
        return True

    def reset(self,time_out=5):
        # if "MT300N-V2" == self.device_type:
        resp = "reset key is pressed\r\n"
        # else:
        #     resp = "reset key is pressed\r\nreset key is pressed\r\n"
        while time_out:
            try:
                res = self.tn.read_until(resp, timeout=5)
            except Exception, e:
                print(e)
                return False
            if resp in res:
                print "reset ", res
                return True
            else:
                time.sleep(0.5)
                time_out -= 1
                print "超时次数：",time_out
        return False
    # 检测led
    def testled(self, time_out=0.3):
        if "GL-MT300N-V2" == self.device_type:
            time_out = 1
        self.tn.write("killall led_wlan_lan_blink 2>/dev/null\n")
        self.tn.write("led on\n")
        time.sleep(time_out)
        self.tn.write("led off\n")
        time.sleep(time_out)
        self.tn.write("led on\n")
        time.sleep(time_out)
        self.tn.write("led off\n")
        time.sleep(time_out)
        self.tn.write("led on\n")
        time.sleep(time_out)
        self.tn.write("led off\n")
        time.sleep(time_out)
        self.tn.write("led on\n")
        time.sleep(time_out)
        self.tn.write("led off\n")
        time.sleep(time_out)
        self.tn.write("led on\n")
        time.sleep(time_out)
        self.tn.write("/etc/rc.local\n")
        res = self.tn.read_until("led indicator", timeout=2)
        print("响应是{}".format(res))
        return True;
    ## 校准检测
    def calibration(self):
        if self.device_type == 'GL-MT300N-V2':
            try:
                self.tn.write("echo $(hexdump /dev/mtd3 -s $((0x58)) -n 1 -C|cut -c 9-12|head -n 1|sed 's/ //g')"+"\n")
                time.sleep(.3)
                v1 = re.findall(r'27', self.tn.read_very_eager())
                self.tn.write("echo $(hexdump /dev/mtd3 -s $((0x5e)) -n 1 -C|cut -c 9-12|head -n 1|sed 's/ //g')"+"\n")
                time.sleep(.3)
                v2 = re.findall(r'27', self.tn.read_very_eager())
                if len(v1) and len(v2):
                    print("设备没有校准")
                    return False
                else:
                    print("设备已经校准")
                    return True
            except:
                return False
        else:
            try:
                if self.device_type == 'GL-AR750':
                    self.tn.write("echo $(hexdump /dev/mtdblock2 -s $((0x108f)) -n 1 -C|cut -c 9-12|head -n 1|sed 's/ //g')"+"\n")
                    time.sleep(.3)
                    v1 =  str(self.tn.read_very_eager()[89:]).replace("\r\n"," ").split(" ")
                    self.tn.write("echo $(hexdump /dev/mtdblock2 -s $((0x1095)) -n 1 -C|cut -c 9-12|head -n 1|sed 's/ //g')"+"\n")
                    time.sleep(.3)
                    v2 = str(self.tn.read_very_eager()[87:]).replace("\r\n"," ").split(" ")
                    self.tn.write("echo $(hexdump /dev/mtdblock2 -s $((0x109b)) -n 1 -C|cut -c 9-12|head -n 1|sed 's/ //g')"+"\n")
                    time.sleep(.3)
                    v3 = str(self.tn.read_very_eager()[87:]).replace("\r\n"," ").split(" ")
                else:
                    self.tn.write("echo $(hexdump /dev/mtdblock6 -s $((0x108f)) -n 1 -C|cut -c 9-12|head -n 1|sed 's/ //g')"+"\n")
                    time.sleep(.3)
                    v1 =  str(self.tn.read_very_eager()[89:]).replace("\r\n"," ").split(" ")
                    self.tn.write("echo $(hexdump /dev/mtdblock6 -s $((0x1095)) -n 1 -C|cut -c 9-12|head -n 1|sed 's/ //g')"+"\n")
                    time.sleep(.3)
                    v2 = str(self.tn.read_very_eager()[87:]).replace("\r\n"," ").split(" ")
                    self.tn.write("echo $(hexdump /dev/mtdblock6 -s $((0x109b)) -n 1 -C|cut -c 9-12|head -n 1|sed 's/ //g')"+"\n")
                    time.sleep(.3)
                    v3 = str(self.tn.read_very_eager()[87:]).replace("\r\n"," ").split(" ")
                if len(v1[1]) and len(v2[1]) and len(v3[1]):
                    print("设备已经校验")
                    return True
                else:
                    print("设备未校验")
                    return False
            except:
                return False

    def testswitch(self, time_out=5):
        try:
            if self.step == "first":
                dsc = 'switch moved to middle'
                while time_out:
                    res = self.tn.read_until(dsc, timeout=5)
                    if dsc in res:
                        return True
                    else:
                        time.sleep(1)
                        time_out -= 1
                        print("测试开关中，超时次数：{}".format(time_out))
                return False
            else:
                if self.device_type == "GL-MT300N-V2" or self.device_type =="GL-AR300M":
                    dsc = 'switch moved to middle\r\nswitch moved to left\r\n'
                else:
                    dsc = 'switch moved to middle\r\nswitch moved to right\r\n'
                while time_out:
                    res = self.tn.read_until(dsc, timeout=5)
                    if dsc in res:
                        return True
                    else:
                        time.sleep(1)
                        time_out -= 1
                        print("测试开关中，超时次数：{}".format(time_out))
                return False
        except:
            return False
    # 测试usb设备
    def testusb(self, time_out=6):
        try:
            while time_out:
                res = self.tn.read_until("find udisk", timeout=5)
                if "find udisk" in res:
                    return True
                else:
                    time.sleep(1)
                    time_out -= 1
                    print("测试usb中，超时次数：{}".format(time_out))
            return False
        except:
            return False

    def testsd(self, time_out=4):
        try:
            while time_out:
                self.tn.write('check_sdcard\n')
                time.sleep(0.3)
                result = str(self.tn.read_very_eager()).replace("\r\n"," ").split(" ")[-3]
                if 'ok' == result:
                    return True
                else:
                    time.sleep(1)
                    time_out -= 1
                    print("测试sd卡中，超时次数：{}".format(time_out))
            return False
        except:
            return False

    def firstok(self, time_out = 5):
        try:
            while time_out:
                self.tn.write("first_test_ok & \n")
                print("wait 1s")
                first = self.tn.read_until("the first test is done, will be able to test second", timeout=2)
                if first:
                    print("return ok from device")
                    return True
                else:
                    time_out -= 1
                    print "写入数据中，次数:"%time_out
            return False
        except:
            return False
    # 向设备写入信息
    def setinfo(self, time_out = 5):
        print "setinfo()"
        cmd = "setinfo {} {} {} {} {} {} {}".format(self.mac_address,self.ddns_name,self.sn_back_up,self.sn_now_use,self.test_name,self.plinfo,self.syst)
        print cmd
        self.tn.write(cmd+'\n')
        res = self.tn.read_until("success", timeout=5)
        print '-*'*20
        print res
        print "-*"*20
        if res == '':
          return False
        return True
    def secondok(self, time_out=5):
        try:
            if config.mods == "product":
                self.setinfo()
                cmd = "second_test_ok\n"
                expect = "the second test is done, will be able to change firmware"
                self.tn.write(cmd)
                res = self.tn.read_until(expect, timeout=5)
                if res == '':
                  return False
                print "second_test_ok"
                cmd = 'cp /root/speed /www/speed.txt\n'
                self.tn.write(cmd)
                if self.device_type == "GL-AR750":
                    cmd = 'dd if=/dev/mtd2 of=/www/art.bin bs=64k\n'
                else:
                    cmd = 'dd if=/dev/mtd6 of=/www/art.bin bs=64k\n'
                self.tn.write(cmd)
                time.sleep(2)
            return True
        except:
            return False

    def checkfirst(self):
        try:
            if "GL-MT300N-V2" == self.device_type:
                self.tn.write("echo $(dd if=/dev/mtd3 bs=1 count=9 skip=$((0x4050)) 2>/dev/null)"+"\n")
                time.sleep(.3)
                v1 = re.findall(r'firsttest',self.tn.read_very_eager())
            elif "GL-AR750" == self.device_type:
                self.tn.write("echo $(dd if=/dev/mtdblock2 bs=1 count=9 skip=80 2>/dev/null)"+"\n")
                time.sleep(.3)
                v1 = re.findall(r'firsttest',self.tn.read_very_eager())
            elif self.device_type in ["GL-AR150","GL-AR300M"]:
                self.tn.write("echo $(dd if=/dev/mtdblock6 bs=1 count=9 skip=80 2>/dev/null)"+"\n")
                time.sleep(.3)
                v1 = re.findall(r'firsttest',self.tn.read_very_eager())
            print v1
            if len(v1):
                print("设备{}已经进行第一次测试".format(self.device_type))
                return True
            else:
                print("设备{}没有进行第一次测试".format(self.device_type))
                return False
        except:
            print "发生错误"
            return False

    def maccompare(self):
        try:
            mac = self.device_tn.send_expect("ifconfig eth0|grep eth0|awk '{print $5}'", "root")
            print mac
            macd = str(mac).replace("\r\n"," ").split(" ")[-3]
            print macd
            print("the sql mac_address is: {}".format(self.mac))
        except Exception,e:
            print e
            try:
                self.device_tn = self.connect_device(host='127.0.0.1')
                if self.device_tn is False:
                    return False
                if self.wait_ping_ip_by_ssh() is False:
                        print('ssh ping失败')
                        return False
                if self.waitdevice() is False:
                    print("连接ssh失败")
                    return False
            except Exception,e:
                print e
                return False
            self.maccompare()

        if cmp(macd, self.mac):
            try:
                mac = self.device_tn.send_expect("ifconfig eth0|grep eth0|awk '{print $5}'", "root")
                print mac
                macs = str(mac).replace("\r\n"," ").split(" ")[-3]
                print macs
                print("the sql mac_address is: {}".format(self.mac))
            except Exception, e:
                print e
                return False
            if cmp(macs,macd):
                print "树莓派的mac地址是:{},二维码传入的mac地址是{},设备的mac地址是{}".format(macs,self.mac,macd)
                return False
            else:
                if self.wait_ping_ip_by_ssh() is False:
                    print('ssh ping失败')
                    return False
                if self.waitdevice() is False:
                    print("连接ssh失败")
                    return False
                self.maccompare()
        else:
            return True

    def wait_wget_success(self, time_out = 10):
        while time_out:
            print " begin wait wget "
            time_out -= 1
            try:
                kill_device_iperf = 'killall -9 iperf_{}'.format(self.device_iperf)
                self.device_tn.send_only(kill_device_iperf)
            except Exception,e:
                print e
                pass
            try:
                self.lan_ip = self.get_inet_ip('wlan0')
                cmd = "wget http://{}/iperf_{} -O /tmp/iperf_{} -T 2".format(self.lan_ip,self.device_iperf,self.device_iperf)
                print "wget cmd is:{}".format(cmd)
                res = self.device_tn.send(cmd,timeout=5)
            except Exception,e:
                print e
                pass
            if "ETA" in res:
                print "wget is succesed"
                return True
            if time_out == 0:
                print "wget is fail"
                return False
            time.sleep(1)

    def speed_rx(self):
        try:
            if self.wait_wget_success() is False:
                return False
            print "wget 得到iperf文件"
            print "-" * 20
            try:
                device_chmod_for_iperf = "chmod +x /tmp/iperf_{}".format(self.device_iperf)
                self.device_tn.send_only(device_chmod_for_iperf)
            except Exception,e:
                print e
                pass
            try:
                self.device_tn.send_only('killall -9 iperf_{}'.format(self.device_iperf))
            except Exception,e:
                print e
                pass
            device_service_iperf = "/tmp/iperf_{} -s &".format(self.device_iperf)
            res = self.device_tn.send_expect(device_service_iperf, "TCP window size:")  # 测试被测设备的RX speed_down
            if res is False:
                return False
            print "-"*20
            print "已经执行服务端"
            self.tn.send_only('killall -9 iperf')
            speed_down = 0
            master_client_iperf = "iperf -c {} -N".format(self.device_ip)
            print "树莓派的命令是:{}".format(master_client_iperf)
            res = self.tn.send_expect(master_client_iperf, "Mbits/sec", timeout=10)  # 25 is 12.5s
            if res is False:
                return False
            print "res is ------------------------------------------\n"
            speed = re.findall("MBytes\s(.* Mbits)", res)[0].strip()
            speed_down = float(re.findall("(.*)M", speed)[0])
            print("speed_down_re is:{} ".format(speed_down))
            print "over"
            # 被测设备杀死该进程
            kill_device_iperf = 'killall -9 iperf_{}'.format(self.device_iperf)
            self.device_tn.send_only(kill_device_iperf)
            time.sleep(0.2)
            clear_buff = self.device_tn.recv()
            print("clear buff begin ===============================")
            print("clear buff: {}".format(clear_buff))
            print("clear buff over ===============================")
            self.speed_rx_re = speed_down
            return speed_down
        except Exception,e:
            print e
            return False

    def write_data_to_device(self):
        try:
            self.device_tn.send_only("echo %s %s > /root/speed"% (self.speed_rx_re, self.speed_tx_re))
            self.device_tn.send_only("exit")
            print("已经写入文件系统")
            return True
        except Exception,e:
            print e
            return False

    def speed_tx(self):
        try:
            master_service_iperf = "iperf -s &"
            self.tn.send_expect(master_service_iperf, "TCP window size:")
            time.sleep(1)
            device_client_iperf = "/tmp/iperf_{} -c {} -N".format(self.device_iperf,self.lan_ip)
            speed_on = 0
            tx = self.device_tn.send_expect(device_client_iperf, "Mbits/sec", timeout=10)
            if tx is False:
                return False
            print("res is --------------------------------------\n")
            print tx
            speed = re.findall("MBytes\s(.* Mbits)", tx)[0].strip()
            speed_on = float(re.findall("(.*)M", speed)[0])
            print("speed_on_re is {}".format(speed_on))
            kill_master_iperf = 'killall -9 iperf'
            self.tn.send_only(kill_master_iperf)
            self.speed_tx_re = speed_on
            return speed_on
        except Exception,e:
            print e
            return False
