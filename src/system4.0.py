#!/usr/bin/env python
#encoding: utf-8
import requests
import logging
import time
import webbrowser
import comm.config as config
import demjson

from modsys4.gliot import KiraClient,Action
from modsys4.sys4 import SystemF

logging.basicConfig(level=logging.DEBUG,
    format = '[%(asctime)s] %(levelname)s %(message)s',
    datefmt = '%Y-%m-%d %H:%M:%S')
root = logging.getLogger()
root.setLevel(logging.NOTSET)

if __name__ == '__main__':
    # 使用默认浏览器打开系统所在的地址。
    box = webbrowser.get('chromium-browser')
    # 打开服务器的地址
    box.open("http://192.168.16.17/dist")
    client = KiraClient(("192.168.16.17",1883), 'gliot')
    client.start()
    # 接受控制端上报在线通知
    @client.route("onlive")
    def handleOnLiveAction(client, action):
        logging.info("handleOneLiveAction()")
        data = demjson.encode({'status': 1})
        actionReply = Action.buildReplyAction(action, "onlive",data)
        client.sendAction(actionReply)
    # 接受控制端连接设备的命令
    @client.route("onconnect")
    def handleOnConnectAction(client, action):
        config.device = SystemF(action)
        if config.device.ping_start is False:
            data = demjson.encode({'status': 0})
        else:
            data = demjson.encode({'status': 1})
        actionReply = Action.buildReplyAction(action, "onconnect",data)
        client.sendAction(actionReply)
    # 接受控制端的测试重置按钮的命令
    @client.route("onreset")
    def handleOnResetAction(client, action):
        if config.device.reset() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "onreset",data)
        client.sendAction(actionReply)
    # 接受控制端的测试gpio的命令
    @client.route("ongpio")
    def handleOnResetAction(client, action):
        if config.device.gpio() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "ongpio",data)
        client.sendAction(actionReply)
    # 接受控制端的测试mesh按钮的命令
    @client.route("onmeshset")
    def handleOnMeshsetAction(client, action):
        if config.device.meshset() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "onmeshset",data)
        client.sendAction(actionReply)
    # 接受控制端的测试led的命令
    @client.route("onled")
    def handleOnLedAction(client, action):
        if config.device.testled() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "onled",data)
        client.sendAction(actionReply)
    # 接受控制端的测试校验状态命令
    @client.route("oncalibration")
    def handleOnCalibrationAction(client, action):
        if config.device.calibration() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "oncalibration",data)
        client.sendAction(actionReply)
    # 接受控制端的测试拨动开关的命令
    @client.route("ontestswitch")
    def handleOnTestSwitchAction(client, action):
        if config.device.testswitch() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "ontestswitch",data)
        client.sendAction(actionReply)
    # 接受控制端的测试usb的命令
    @client.route("ontestusb")
    def handleOnTestUsbAction(client, action):
        if config.device.testusb() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "ontestusb",data)
        client.sendAction(actionReply)
    # 接受控制端的测试sd卡的命令
    @client.route("ontestsd")
    def handleOnTestSd(client, action):
        if config.device.testsd() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "ontestsd",data)
        client.sendAction(actionReply)
    # 接受控制端的测试内存的命令
    @client.route("ontestddr")
    def handleOnTestSd(client, action):
        if config.device.checkMem() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "ontestddr",data)
        client.sendAction(actionReply)
    # 接受控制端的测试3g卡的命令
    @client.route("ontest3g")
    def handleOnTestSd(client, action):
        if config.device.test3g() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "ontest3g",data)
        client.sendAction(actionReply)
    # 接受控制端的完成pcba测试的命令
    @client.route("onfirstok")
    def handleOnFirstOkAction(client, action):
        if config.device.firstok() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "onfirstok",data)
        client.sendAction(actionReply)
    # 接受控制端的完成core版的pcba测试的命令
    @client.route("oncoreok")
    def handleOnFirstOkAction(client, action):
        if config.device.coreok() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "oncoreok",data)
        client.sendAction(actionReply)
    # 接受控制端的完成product测试的命令
    @client.route("onsecondok")
    def handleOnSecondOkAction(client,action):
        if config.device.secondok() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "onsecondok",data)
        client.sendAction(actionReply)
    # 接受控制端的检测pcba测试完成标志的命令
    @client.route("oncheckfirst")
    def handleOnCheckFirstAction(client, action):
        if config.device.checkfirst() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "oncheckfirst",data)
        client.sendAction(actionReply)
    # 接受控制端的吞吐量测试前设备的预处理「即设置ssid和连接wifi的测试前的处理功过」命令
    @client.route("onpretreat")
    def handleOnPreTreatAction(client, action):
        if config.device.pretreat(action.mac_address) is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "onpretreat",data)
        client.sendAction(actionReply)
    # 接受控制端的mac地址比较「数据库mac地址和自己真实mac地址」的命令
    @client.route("onmacpare")
    def handleOnMacCompareAction(client, action):
        if config.device.maccompare() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "onmacpare",data)
        client.sendAction(actionReply)
    # 接受控制端的控制电源控制板的命令
    @client.route("onpower")
    def handleOnPowerAction(client, action):
        if config.device.control_power(action.count) is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "onpower",data)
        client.sendAction(actionReply)
    # 接受控制端测试吞吐量值的rx值的命令
    @client.route("onspeedrx")
    def handleOnSpeedRxAction(client, action):
        speed = config.device.speed_rx()
        if speed is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'speed_rx': speed, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "onspeedrx",data)
        client.sendAction(actionReply)
    # 接受控制端测试吞吐量值的tx值的命令
    @client.route("onspeedtx")
    def handleOnSpeedTxAction(client, action):
        speed = config.device.speed_tx()
        if speed is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'speed_tx': speed, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "onspeedtx",data)
        client.sendAction(actionReply)
    # 接受控制端离线的命令
    @client.route("ondie")
    def handleOnDieAction(client, action):
        if config.device.offline() is False:
            data = demjson.encode({'status': 0, 'index': action.index})
        else:
            data = demjson.encode({'status': 1, 'index': action.index})
        actionReply = Action.buildReplyAction(action, "ondie",data)
        client.sendAction(actionReply)

    while True:
         time.sleep(1)
