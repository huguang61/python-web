#!/usr/bin/env python
# -*- coding: utf-8 -*-
from comm.comm import *
import re

def first_test_ok(tn, browser, time_out=5):
    while time_out:
        tn.write("first_test_ok & \n")
        print "wait 1s"
        time.sleep(1)
        print "wait tesk success return from device"
        first = tn.read_until(
            "the first test is done, will be able to test second", timeout=1)
        if first:
            print "return ok from device"
            js = '$(\"#button-default\").text(\"测试完成，请更换路由器\")'
            browser.execute_script(js)
            return True
        else:
            time_out -= 1
            print "写入数据中，次数:" % time_out
    return False


def test_sdcard(tn, browser, time_out=5):
    js = 'document.getElementById("button-sd").style.display=""'
    browser.execute_script(js)
    while time_out:
        tn.write('check_sdcard\n')
        time.sleep(0.5)
        results = tn.read_until("sdcard ok\n", timeout=2)
        print "___________________"
        print results
        print "____________________"
        matchObj = re.search( r'ok', results, re.M|re.I)
        if matchObj:
            print "check_sdcard success"
            js = '$(\"#button-sd\").text(\"sd卡测试成功\")'
            browser.execute_script(js)
            return True
        time_out -= 1
        time.sleep(1)
        print "剩余测试为{}".format(time_out)
    js = '$(\"#button-sd\").text(\"sd卡测试失败\")'
    browser.execute_script(js)
    return False


def first_test_led_key_usb(tn, browser, type):
    tn.write("version\n")
    version = tn.read_until("version\n", timeout=2)
    if version is False:
        print "version test Fail"
        return False
    if test_reset_tel(tn, browser, type) is False:
        print "测试重置开关失败"
        return False
    test_led_tel(tn, browser, type)
    print "type is ", type
    if "GL-MT300N-V2" == type:
        if test_switch_tel_mt300N_v2(tn, browser, type) is False:
            js = '$(\"#button-test\").text(\"测试开关失败\")'
            browser.execute_script(js)
            print "测试开关失败"
            return False
    elif "GL-AR750" == type:
        print "GL-AR750 开关开始测试"
        if swith_for_ar750(tn,browser) is False:
            js = '$(\"#button-test\").text(\"测试开关失败\")'
            browser.execute_script(js)
            print "测试开关失败"
            time.sleep(10)
            return False
    elif "GL-MIFI" == type:
        test_sd_3g(tn, browser)
        pass
    else:
        if test_switch_tel(tn, browser, type) is False:
            print "测试开关失败"
            return False
    if "GL-AR750" == type:
        print "GL-AR750 sd卡开始测试"
        if test_sdcard(tn, browser) is False:
            print "测试sd卡失败"
            return False
    if test_USP_tel(tn, browser) is False:
        print "测试usb失败"
        return False
    if first_test_ok(tn, browser):
        js = '$(\"#button-default\").text(\"测试完成，请断电\")'
        browser.execute_script(js)
        tn.close()
        return True
    else:
        tn.close()
        return False
