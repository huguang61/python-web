#!/usr/bin/env python
# -*- coding: utf-8 -*-
from comm.comm import *
## 获取运行状态值
def get_second_test_status(bow):
  name = bow.find_element_by_id("button-second").text
  return name
## mt-300n-v2是否通过第一测试检测
def mt_300n_v2_first(tn,bow):
  tn.write("echo $(dd if=/dev/mtd3 bs=1 count=9 skip=$((0x4050)) 2>/dev/null)"+"\n")
  time.sleep(.1)
  v1 = re.findall(r'firsttest',tn.read_very_eager())
  if len(v1):
    print "设备已经进行第一次测试"
    return True
  else:
    print "设备没有进行第一次测试"
    js = '$(\"#button-second\").text(\"请先进行第一次测试!\")'
    bow.execute_script(js)
    time.sleep(20)
    return False
# ar_750是否通过第一次检测
def ar_750_first(tn,bow):
  tn.write("echo $(dd if=/dev/mtdblock2 bs=1 count=9 skip=80 2>/dev/null)"+"\n")
  time.sleep(.1)
  v1 = re.findall(r'firsttest',tn.read_very_eager())
  if len(v1):
    print "设备已经进行第一次测试"
    return True
  else:
    print "设备没有进行第一次测试"
    js = '$(\"#button-second\").text(\"请先进行第一次测试!\")'
    bow.execute_script(js)
    time.sleep(20)
## 检测设备是否通过第一次测试
def first_test_is_ok(tn,bow):
  tn.write("echo $(dd if=/dev/mtdblock6 bs=1 count=9 skip=80 2>/dev/null)"+"\n")
  time.sleep(.1)
  v1 = re.findall(r'firsttest',tn.read_very_eager())
  if len(v1):
    print "设备已经进行第一次测试"
    return True
  else:
    print "设备没有进行第一次测试"
    js = '$(\"#button-second\").text(\"请先进行第一次测试!\")'
    bow.execute_script(js)
    time.sleep(20)
    return False

def set_rou_info_disabled(bow):
  js = "document.getElementById(\"rou_info\").setAttribute(\"disabled\", \"disabled\")"
  bow.execute_script(js)

def switch_code_useed(browser):
  while True:
    go_on = browser.find_element_by_id("button-second").text
    if go_on == u"开始第二次测试...":
      js = '$("#button-clear").hide()'
      browser.execute_script(js)
      return True
    go_back = browser.find_element_by_id("button-clear").text
    if go_back == u"重新扫码":
      js = '$(\"#button-second\").text(\"code已被占用，请重新扫码，等待即可\")'
      browser.execute_script(js)
      js = '$(\"#rou_info\").val("")'
      browser.execute_script(js)
      js = '$(\"#button-clear\").text(\"点击重新扫码\")'
      browser.execute_script(js)
      js = '$("#button-clear").hide()'
      browser.execute_script(js)
      return False
    time.sleep(1)
# 检查二维码的占用情况
def check_code_used(ddns_name, file_manufacturer_name,file_customer_name,test_name,browser):
  js = '$(\"#button-second\").text(\"正在检查code使用情况\")'
  browser.execute_script(js)
  time.sleep(0.5)
  upload_info = "{}:{}:{}:{}".format(ddns_name, file_manufacturer_name, file_customer_name, test_name)
  res = requests.get("http://192.168.16.17/index.php?upload_info=" + upload_info)
  if res.status_code != 200:
    print "get error", res.status_code, res.text
    return False
  info = str(res.text)[:-1]  # 除去结尾的换行符
  print 'info is:',info
  if 'Undefined' in info:
    print "数据库中没有该设备信息"
    js = '$(\"#button-second\").text(\"数据库中没有该设备信息\")'
    browser.execute_script(js)
    time.sleep(5)
    return False, False
  if info[-1] != "0":
    print "info[-1] is ", info[-1]
    js = '$("#rou_info").show()'
    browser.execute_script(js)
    js = '$(\"#button-clear\").text(\"重新扫码\")'
    browser.execute_script(js)
    js = 'document.getElementById("button-clear").style.display=""'
    browser.execute_script(js)
    if switch_code_useed(browser) is False:  # 重新扫码
      print "重新扫码"
      time.sleep(5)
      return False,False
      # 继续测试
  js = "document.getElementById(\"button-second\").setAttribute(\"class\",\"btn btn-lg btn-success\");"
  browser.execute_script(js)
  return upload_info,info

def first_io_data_check(upload_info, info, scan_get_code_service):
  mac, tmp, tmp, server_get_code_service, server_manufacturer_name, \
  server_customer_name, server_test_name, count = info.split()
  ddns_name, file_manufacturer_name, file_customer_name, file_test_name = upload_info.split(":")
  if scan_get_code_service != server_get_code_service or \
    server_manufacturer_name[:2] != file_manufacturer_name or\
    server_customer_name != file_customer_name or\
    server_test_name != file_test_name:
    print scan_get_code_service, " - ", server_get_code_service
    print server_manufacturer_name, '-', file_manufacturer_name
    print server_customer_name, "-", file_customer_name
    print server_test_name, '-', file_test_name
    return False
  return True
## 第二轮测试初始化
def continue_second_test(browser):
  js = '$(\"#button-reset\").text(\"请按下复位按钮\")'
  browser.execute_script(js)
  js = '$(\"#button-test\").text(\"请拨动开关至少两个来回\")'
  browser.execute_script(js)

  js = '$("#rou_info").val("")'
  browser.execute_script(js)
  js = '$("#button-second").text("第二次测试")'
  browser.execute_script(js)
  js = "document.getElementById(\"button-second\").setAttribute(\"class\",\"btn btn-lg btn-danger\");"
  browser.execute_script(js)
  js = '$("input").attr("disabled", false)'
  browser.execute_script(js)
  js = '$("#button-reset").hide()'
  browser.execute_script(js)
  js = '$("#button-meshset").hide()'
  browser.execute_script(js)
  js = '$("#button-test").hide()'
  browser.execute_script(js)
  js = '$("#rou_info").focus()'
  browser.execute_script(js)
  pass
## 设置信息导mt300n_v2
def set_info_tel_mt300n_v2(tn, info):
  info = info[:-2]  # 除去结尾use 次数 和空格
  # res = ssh.send("setinfo "+info, timeout=2)
  print "seting ..."
  print "cmd is : setinfo " + info
  # res = ssh.send_expect(cmd="setinfo " + info, expect="set macaddr and sn success", timeout=30)  # 30*0.3=9s
  tn.write("setinfo "+info + "\n")
  res = tn.read_until("set macaddr and sn success", timeout=5)
  if res == '':
    return False
  print res
  print "Set info success "
  cmd = "second_test_ok\n"
  expect = "the second test is done, will be able to change firmware"
  tn.write(cmd)
  res = tn.read_until(expect, timeout=5)
  if res == '':
    return False
  print "second_test_ok"
  cmd = 'cp /root/speed /www/speed.txt\n'
  tn.write(cmd)
  cmd = 'dd if=/dev/mtd3 of=/www/art.bin bs=64k\n'
  tn.write(cmd)
  time.sleep(2)
  return True

def set_info_tel(tn, info):
  info = info[:-2]  # 除去结尾use 次数 和空格
  # res = ssh.send("setinfo "+info, timeout=2)
  print "seting ..."
  print "cmd is : setinfo " + info
  # res = ssh.send_expect(cmd="setinfo " + info, expect="set macaddr and sn success", timeout=30)  # 30*0.3=9s
  tn.write("setinfo "+info + "\n")
  res = tn.read_until("set macaddr and sn success", timeout=5)
  if res == '':
    return False
  print res
  print "Set info success "
  cmd = "second_test_ok\n"
  expect = "the second test is done, will be able to change firmware"
  tn.write(cmd)
  res = tn.read_until(expect, timeout=5)
  if res == '':
    return False
  print "second_test_ok"
  cmd = 'cp /root/speed /www/speed.txt\n'
  tn.write(cmd)
  cmd = 'dd if=/dev/mtd6 of=/www/art.bin bs=64k\n'
  tn.write(cmd)
  time.sleep(2)
  return True
# 计算下一个mac地址：
def computemac(macaddr):
  last_two = "".join(macaddr.split(":"))
  last_two_int = int(last_two,16)
  new_last_two_int = last_two_int + 1
  new_last_two = hex(new_last_two_int)
  new_last_two = new_last_two[2:-1]
  for i in range(len(new_last_two),12):
    new_last_two = '0'+str(new_last_two)
  new_addr = ""
  for item in range(1,13):
    if item % 2 == 0:
      if item == 12:
        new_addr = new_addr + new_last_two[item-2:item]
      else:
        new_addr =new_addr + new_last_two[item-2:item]+":"
  return new_addr.upper()
def second_test_new(browser, code, device_ip, type, tn, os_type):
  set_rou_info_disabled(browser)
  file_manufacturer_name = browser.find_element_by_id("manufacturer_name").get_attribute("value")
  file_customer_name = browser.find_element_by_id("customer_name").get_attribute("value")
  test_name = getpass.getuser()
  code_tuple = split_code(code)
  ddns_name, scan_get_code_service = get_two_par_from_code_tuple(code_tuple)
  if ddns_name is False:
    print "查找ddns失败"
    js = '$(\"#button-second\").text(\"查找ddns失败\")'
    browser.execute_script(js)
    return False
  upload_info,info = check_code_used(ddns_name,file_manufacturer_name,file_customer_name,test_name,browser)
  if upload_info is False:
    return False
  if "GL-AR750" == type:
    macd = info.split()
    next_mac = computemac(macd[0])
    print next_mac
    code_tuple = split_code(next_mac)
    print code_tuple
    ddns_name1, scan_get_code_service1 = get_two_par_from_code_tuple(code_tuple)
    print ddns_name1
    upload_info1,info1 = check_code_used(ddns_name1,file_manufacturer_name,file_customer_name,test_name,browser)
    print upload_info1
    if upload_info1 is False:
      return False
  if first_io_data_check(upload_info, info, scan_get_code_service) is False:
    print upload_info, scan_get_code_service
    print info
    js = '$(\"#button-second\").text(\"数据库获取数据与上传数据匹配失败!\")'
    browser.execute_script(js)
    return False
  js = '$(\"#button-second\").text(\"正在连接路由器\")'
  browser.execute_script(js)
  if tn is False:
    return False
  if test_reset_tel(tn, browser, type) is False:
    print "测试重置开关失败"
    return False
  # switch
  if "MT300N-V2" in type:
    if switch_miwifi_in_shell_mt300n_v2(tn, browser) is False:
      print "测试开关失败"
      return False
  elif "MIFI" in type:
    pass
  elif "300M" in type:
    if switch_miwifi_in_shell_300m(tn, browser) is False:
      print "测试重置开关失败"
      return False
  elif "GL-AR750" == type:
    if swith_for_ar750(tn,browser) is False:
      print "测试重置开关失败"
      return False
  else:
    if switch_miwifi_in_shell(tn, browser) is False:
      print "测试重置开关失败"
      return False

  js = '''$(\"#button-second\").text(\"测试完成，等待写入信息\")'''
  browser.execute_script(js)
  if "MT300N-V2" in type:
    if set_info_tel_mt300n_v2(tn, info) is False:
      print "写入信息失败"
      return False
  else:
    if set_info_tel(tn, info) is False:
      print "写入信息失败"
      return False
  return True
